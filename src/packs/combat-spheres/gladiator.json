{
  "_id": "z8ONCrj48gAS3S44",
  "name": "Gladiator",
  "folder": null,
  "sort": 0,
  "flags": {
    "pf1spheres": {
      "sphere": "gladiator"
    }
  },
  "pages": [
    {
      "name": "Gladiator",
      "type": "text",
      "title": {
        "show": false,
        "level": 1
      },
      "text": {
        "format": 1,
        "content": "<img src=\"modules/pf1spheres/assets/icons/spheres/gladiator.webp\" style=\"float:right;border:none;outline:none\" />Practitioners of the Gladiator sphere are experts at fighting with flair and style in order to manipulate the emotions and actions of their enemies, their allies, or the crowd. The gladiator understands that winning is a mental game; if you can convince your enemy that you should be feared, the battle is half-finished already. When you gain the Gladiator sphere, you gain 5 ranks in the Intimidate skill, plus 5 ranks per additional talent spent in the Gladiator sphere (maximum ranks equal to your total Hit Dice). If you already have ranks in Intimidate you may immediately retrain them, but you do not get to retrain when only temporarily gaining talents, such as through the armiger’s customized weapons class feature.\n<h2 id=\"toc0\"><span><span style=\"color:#993300\">Boast</span></span></h2>\n<p>As long as you have martial focus, after confirming a critical hit, reducing an enemy to 0 or fewer hit points, or succeeding on a combat maneuver, you may perform a boast as an immediate action. Talents with the (boast) descriptor grant additional options for your boast. Each boast may only use one of these talents. Some boasts affect other creatures; a creature must be able to see or hear you and have an Intelligence score (not Intelligence (-)) to be affected. Boasts have a range of close (25 ft. + 5 ft. per 2 ranks in Intimidate you possess). When you gain the Gladiator sphere, you learn the following boast:</p>\n<h4 id=\"toc1\"><span>Prowess (boast)</span></h4>\n<p>You may roll the next weapon attack you make before the end of your next turn twice and take the better result.</p>\n<h2 id=\"toc2\"><span><span style=\"color:#993300\">Demoralization</span></span></h2>\n<p>Some talents in this sphere have effects that allow new ways to make Intimidate skill checks to demoralize enemies or grant new options against demoralized foes. These talents carry the (demoralization) descriptor. When you gain the Gladiator sphere, you learn the following (demoralization) ability:</p>\n<h4 id=\"toc3\"><span>Strike Fear (demoralization)</span></h4>\n<p>As a full-round action, you may expend martial focus to make an Intimidate check to demoralize all targets within 30 ft. of you who can both see and hear you. You may choose to take a -10 penalty on the check; if you do so you do not have to expend martial focus. <strong>Associated Feat:</strong> Dazzling Display.</p>\n<hr /><br />\n<br /><b>Gladiator Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.combat-talents.LCTyn3yfQVy7icBF]{Derision}</li>\n<li>@Compendium[pf1spheres.combat-talents.0wECNV1FGeg94j6I]{Fan Favorite}</li>\n<li>@Compendium[pf1spheres.combat-talents.qypvIxCIaz8HxIhE]{Master Of Fear}</li>\n<li>@Compendium[pf1spheres.combat-talents.EqEOw463M5UObQiN]{Motivational Audience}</li>\n<li>@Compendium[pf1spheres.combat-talents.5QGdtx2WdbprbZWQ]{Pathetic Yelp}</li>\n<li>@Compendium[pf1spheres.combat-talents.zSlmjsQyiT8ZpKGu]{Punish The Meek}</li>\n<li>@Compendium[pf1spheres.combat-talents.qkJHdqlQLLbqej5R]{Self Confidence}</li>\n<li>@Compendium[pf1spheres.combat-talents.5x9eNQ4CVXc7uCOy]{Theatrical Boast}</li>\n<li>@Compendium[pf1spheres.combat-talents.L06m2mTIh5kIxgzt]{Trash Talker}</li>\n<li>@Compendium[pf1spheres.combat-talents.yZTAEBm4OMqqkeqB]{Uncowed}</li>\n<li>@Compendium[pf1spheres.combat-talents.xLQgOSsOE4TOTPsP]{Vengeful Boast}</li>\n</ul><br /><b>Boast Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.combat-talents.wvaOB82hmjYI7AWi]{Battering Banter}</li>\n<li>@Compendium[pf1spheres.combat-talents.ZVPtimSBBGxxmkFS]{Bloodthirst}</li>\n<li>@Compendium[pf1spheres.combat-talents.84yi81jqcMzOvFvb]{Caper}</li>\n<li>@Compendium[pf1spheres.combat-talents.eQw6H7OitGtchcod]{Distracting Display}</li>\n<li>@Compendium[pf1spheres.combat-talents.ZXlWkLlK34mXybES]{Enthusiastic Cheer}</li>\n<li>@Compendium[pf1spheres.combat-talents.yZCZxql09SWKysvd]{Exemplar}</li>\n<li>@Compendium[pf1spheres.combat-talents.1gsqyiha3yIKdvtr]{Flamboyant Movement}</li>\n<li>@Compendium[pf1spheres.combat-talents.aT6lliyqzG276P85]{Inspire}</li>\n<li>@Compendium[pf1spheres.combat-talents.ad89eAalTTGCWBzF]{Inspiring Pose}</li>\n<li>@Compendium[pf1spheres.combat-talents.ZCbbgSo8eFtPxG1j]{Iron Roar}</li>\n<li>@Compendium[pf1spheres.combat-talents.UgKd0Y4fVviwFov9]{Last Bastion}</li>\n<li>@Compendium[pf1spheres.combat-talents.Jx1KIISEMmIm4SGe]{Menace}</li>\n<li>@Compendium[pf1spheres.combat-talents.P6aOgGdfyPo0niiA]{Rattle Confidence}</li>\n<li>@Compendium[pf1spheres.combat-talents.Pi3Viabz7nNZmDR1]{Spur Violence}</li>\n<li>@Compendium[pf1spheres.combat-talents.x5Z7TOl5h2HGVgOp]{Steel Braggart}</li>\n<li>@Compendium[pf1spheres.combat-talents.vkeSfwrkVLnRuEFd]{Unsettling Visage}</li>\n</ul><br /><b>Demoralization Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.combat-talents.DyGCrn8XyjqAvS1C]{Abandon All Hope}</li>\n<li>@Compendium[pf1spheres.combat-talents.Yk6OFidSWWElZW5L]{Cow Enemy}</li>\n<li>@Compendium[pf1spheres.combat-talents.HKDVHd47jR80b97G]{Cowing Nightmare}</li>\n<li>@Compendium[pf1spheres.combat-talents.ZHRLduSCIUBlfCeS]{Coward’s Bane}</li>\n<li>@Compendium[pf1spheres.combat-talents.lg3vcDByCWCop60P]{Daunting}</li>\n<li>@Compendium[pf1spheres.combat-talents.lTwEANkMhDlzfayt]{Dread}</li>\n<li>@Compendium[pf1spheres.combat-talents.RfNWECBMrTDQOKNl]{Duel Of Wills}</li>\n<li>@Compendium[pf1spheres.combat-talents.YW3XZpNQXnkYHGtI]{Dullahan’s Call}</li>\n<li>@Compendium[pf1spheres.combat-talents.y7Ayg0o5ZQO0N7Ca]{Fear Eater}</li>\n<li>@Compendium[pf1spheres.combat-talents.BWSVSFZDMJ7KeLu2]{Frightful}</li>\n<li>@Compendium[pf1spheres.combat-talents.7YAx72S5slw0gArR]{Hear Their Screams}</li>\n<li>@Compendium[pf1spheres.combat-talents.NB5QqpgHeOPde5fB]{Murderous Intent}</li>\n<li>@Compendium[pf1spheres.combat-talents.ckvR9BUj3cPT623D]{Nightmarish Microcosm}</li>\n<li>@Compendium[pf1spheres.combat-talents.Dz6aWZkv0GvF2D8B]{Piercing Fear}</li>\n<li>@Compendium[pf1spheres.combat-talents.UlUKTa6lWwGYaKcE]{Shaken Defense}</li>\n<li>@Compendium[pf1spheres.combat-talents.3AASNDeGpLK4bgor]{Spectacle}</li>\n</ul><br /><b>Legendary Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.combat-talents.RMlHxbPvN56itFWY]{Aura Of Fear}</li>\n<li>@Compendium[pf1spheres.combat-talents.eBQyN4iREVA1GUfn]{Burn The Chaff}</li>\n<li>@Compendium[pf1spheres.combat-talents.tca8Rf3WmXOWW2SB]{Deafening Clangor}</li>\n<li>@Compendium[pf1spheres.combat-talents.ThLDQ6Ho1XWGrWLk]{Draining Despair}</li>\n<li>@Compendium[pf1spheres.combat-talents.wsqLedJdiDCJa2zV]{Final Stand}</li>\n<li>@Compendium[pf1spheres.combat-talents.IeU3Ecer81qqVBsq]{Foresee Conflict}</li>\n<li>@Compendium[pf1spheres.combat-talents.ntsdNqL7R5r65eIt]{Incredible Inspiration}</li>\n<li>@Compendium[pf1spheres.combat-talents.ImLQlGAmuux2SoFp]{Nightmare Fuel}</li>\n<li>@Compendium[pf1spheres.combat-talents.0I5aRebfgxtjBMh6]{Nightmare Stalker}</li></ul>"
      },
      "_id": "dGgOjHSwvNV7wTP1",
      "image": {},
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "sort": 0,
      "ownership": {
        "default": -1
      },
      "flags": {}
    }
  ],
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "0.82.2",
    "coreVersion": "10.285",
    "createdTime": 1663169292943,
    "modifiedTime": 1663181691631,
    "lastModifiedBy": "pf1spheres-db000"
  }
}
