{
  "_id": "dlYaO3uxz2sBW5IW",
  "name": "Lancer",
  "folder": null,
  "sort": 0,
  "flags": {
    "pf1spheres": {
      "sphere": "lancer"
    }
  },
  "pages": [
    {
      "name": "Lancer",
      "type": "text",
      "title": {
        "show": false,
        "level": 1
      },
      "text": {
        "format": 1,
        "content": "<img src=\"modules/pf1spheres/assets/icons/spheres/lancer.webp\" style=\"float:right;border:none;outline:none\" />All practitioners of the Lancer sphere gain the following ability:\n<h2 id=\"toc0\"><span><span style=\"color:#993300\">Impale</span></span></h2>\n<p>When making a melee attack with the attack action that deals lethal damage, you can take a -2 penalty on the attack roll; if the attack is successful, your weapon impales the creature, forcing it into a square within your reach if it was not already. An impaled creature cannot move and is battered for as long as it remains impaled. An impaled creature who attempts to cast a spell or use a spell-like ability must make a concentration check (DC 10 + your CMB + spell level), or lose the spell. Weapons used to impale a creature cannot be used for attacks except for those against the impaled creature (you cannot impale more than a single creature with your unarmed strikes), ignoring armor, natural armor, and shield bonuses to AC. A creature can only be impaled by a single weapon; attempting to impale an already impaled creature automatically fails. Controlling an impaling weapon requires the same amount of hands as wielding the weapon does.</p>\n<p>This impalement may be broken when you lose control of the weapon used for the attack (such as by being disarmed), or by the impaled creature making a combat maneuver check as a standard action with a DC equal to your CMD. If you control the weapon being used to impale a creature, you may choose to automatically remove the weapon as a move action. When the impaled creature loses the impaled condition, they take bleed damage equal to the damage dice of the weapon used to impale the creature. If a creature is impaled by a weapon that is not controlled by another creature, they are able to move, but all movement speeds they possess are reduced by 1/2 (minimum 5 ft.).</p>\n<p>You must make an opposed Strength check against the impaled creature to move as a part of the move action (this Strength check is made by your mount if you are mounted), moving up to half of your base speed while the impaled creature is dragged along with you (this movement does not provoke attacks of opportunity).</p>\n<p>If you are impaling two or more creatures, you must make an opposed Strength check against all currently impaled creatures in order to move, and if you are subjected to forced movement, you must make an opposed Strength check against all impaled creature; failing this check against any impaled creature forces you to release any weapons you’re using to impale a creature (or to end impaling a creature with your unarmed strikes or natural attacks) immediately, while success drags any impaled creatures with you during this forced movement.</p>\n<p>You can also choose to release the weapon as a free action without dealing damage (although you cannot release natural weapons). A creature may attempt a grapple check against an impaled creature’s CMD to assume control of the weapon impaling that creature as long as another creature is not controlling it. If so, the creature making the attempt receives a +4 bonus on the grapple check.</p>\n<p>Talents with the (impale) tag can only be used against creatures who are impaled by this ability when you are controlling the impaling weapon.</p>\n<hr /><br />\n<br /><b>Lancer Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.combat-talents.wRKuN7gjhV7yTCT8]{Adamant Stalker}</li>\n<li>@Compendium[pf1spheres.combat-talents.Azwsv8tfG77w9eGM]{Bracing Pierce}</li>\n<li>@Compendium[pf1spheres.combat-talents.zajrWaPn4qOk0dUM]{Defensive Leverage}</li>\n<li>@Compendium[pf1spheres.combat-talents.laVl25AG1whjj4Cw]{Double Impale}</li>\n<li>@Compendium[pf1spheres.combat-talents.gNHLN2Ne5f43omyT]{Focusing Finale}</li>\n<li>@Compendium[pf1spheres.combat-talents.YhVguHzA9wdj1fkx]{Lancealot}</li>\n<li>@Compendium[pf1spheres.combat-talents.XLzAfFbMrZO9JLGO]{Opportune Impalement}</li>\n<li>@Compendium[pf1spheres.combat-talents.QLly19ZixEklz7DU]{Pincushion Punishment}</li>\n<li>@Compendium[pf1spheres.combat-talents.5EfT3v99DbEicCH3]{Pinning Impale}</li>\n<li>@Compendium[pf1spheres.combat-talents.E08SgpaM4Op1bkSh]{Ranged Impale}</li>\n<li>@Compendium[pf1spheres.combat-talents.qDUNk2NYYOTXXFWI]{Terrifying Pierce}</li>\n<li>@Compendium[pf1spheres.combat-talents.7sz6JTy6KHw6NyrK]{Whirlwind Knockdown}</li>\n</ul><br /><b>Impale Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.combat-talents.SZ6CFoGZGuPy9kFi]{Bloody Rip}</li>\n<li>@Compendium[pf1spheres.combat-talents.fhuANeAz0KC21y00]{Cruel Vibration}</li>\n<li>@Compendium[pf1spheres.combat-talents.njmn37EsOxBaqqsD]{Distracting Tear}</li>\n<li>@Compendium[pf1spheres.combat-talents.DYEIStwOVQ20BlpI]{Human Shield}</li>\n<li>@Compendium[pf1spheres.combat-talents.ViPBtun6Ljgn3Mti]{Painful Twist}</li>\n<li>@Compendium[pf1spheres.combat-talents.HexyUlhrp1fLYHdc]{Piercing Heft}</li>\n<li>@Compendium[pf1spheres.combat-talents.lInhwfG6YrhmySnO]{Ragdoll Swing}</li>\n<li>@Compendium[pf1spheres.combat-talents.oam45sDXWMxr8EoK]{Staggering Tear}</li>\n<li>@Compendium[pf1spheres.combat-talents.5UrMqwkz7qy6Vkhs]{Unbalancing Twist}</li>\n</ul><br /><b>Legendary Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.combat-talents.qvyo7IfJCERiKqal]{Dimensional Pierce}</li>\n<li>@Compendium[pf1spheres.combat-talents.9nitK98eutdIH49a]{Esoteric Link}</li>\n<li>@Compendium[pf1spheres.combat-talents.tjpjeD3Fwm7KDxgN]{Pierce Vitals}</li>\n<li>@Compendium[pf1spheres.combat-talents.kbaXFQ9lkWY0XEVS]{Soul Link}</li>\n<li>@Compendium[pf1spheres.combat-talents.VdACZwk0cEAsdUNJ]{Soul Pierce}</li></ul>"
      },
      "_id": "R3Pzw2eTyaM7CKFV",
      "image": {},
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "sort": 0,
      "ownership": {
        "default": -1
      },
      "flags": {}
    }
  ],
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "0.82.2",
    "coreVersion": "10.285",
    "createdTime": 1663169292938,
    "modifiedTime": 1663181691530,
    "lastModifiedBy": "pf1spheres-db000"
  }
}
