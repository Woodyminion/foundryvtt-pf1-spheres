{
  "_id": "sOrAhIPryz10PgJz",
  "name": "Recruit",
  "type": "feat",
  "img": "modules/pf1spheres/assets/icons/spheres/leadership.webp",
  "effects": [],
  "folder": null,
  "sort": 0,
  "flags": {
    "pf1spheres": {
      "sphere": "leadership"
    }
  },
  "system": {
    "description": {
      "value": "<p>You may recruit adventurers, mercenaries, and sellswords to actively assist you. Recruiting requires spending 8 hours in a settlement and making a Diplomacy check. You may select the profession and martial tradition you are looking for, and may also specify a particular race from those available. Not all races, professions, and traditions will be available in all locations; the available pool is determined by the GM. The base DC of this check is 10 + the Hit Dice of the person you are looking to recruit, modified per Settlement Size Modifier and Miscellaneous Modifier tables. Modifiers are cumulative. You may take 10 on this check.</p>\n<p>You may not recruit a creature with more Hit Dice than the maximum shown on Table: Cohort. The exact feats and talents the cohort possesses are up to the GM, though you may reject a given cohort and attempt to recruit another with the normal action. Recruits are usually humanoids with no racial Hit Dice, and are of races available in the region you are recruiting. Creatures with racial Hit Dice, templates, or abilities unsuitable to player races are not suitable to be cohorts.</p>\n<p>You may have multiple recruited creatures under your control, but their total Hit Dice cannot exceed your ranks in Diplomacy. If you attempt to recruit a creature that would exceed your Hit Dice cap, you must choose which other creatures to release from your service.</p>\n<p>A cohort serves you loyally and will follow you into and aid you during combat, but will not obey obviously suicidal orders nor subject itself to harm for no obvious purpose. Cohorts are usually within 1 alignment step of you, though some exceptions may occur at the GM&#8217;s discretion.</p>\n<p><sup><strong>Wiki Note:</strong> Effects that increase effective BAB or skill ranks in a sphere do not accelerate the strength of leadership cohorts or similar partners, just as increasing your caster level does not accelerate the hit dice of Conjuration sphere companions. Only effects that actually raise your effective level, such as the Spherebound mythic quality or a Conscript sphere specialization, can increase the hit dice of cohorts and similar companions.</sup></p>\n<strong>Table: Settlement Size Modifiers</strong><br>\n<table class=\"wiki-content-table\">\n<tr>\n<th>Population</th>\n<th>Settlement Size</th>\n<th>DC Modifier</th>\n</tr>\n<tr>\n<td>Fewer than 21</td>\n<td>Thorp</td>\n<td>+8</td>\n</tr>\n<tr>\n<td>21&#8211;60</td>\n<td>Hamlet</td>\n<td>+6</td>\n</tr>\n<tr>\n<td>61&#8211;200</td>\n<td>Village</td>\n<td>+4</td>\n</tr>\n<tr>\n<td>201&#8211;2,000</td>\n<td>Small town</td>\n<td>+2</td>\n</tr>\n<tr>\n<td>2,001&#8211;5,000</td>\n<td>Large town</td>\n<td>+1</td>\n</tr>\n<tr>\n<td>5,001&#8211;10,000</td>\n<td>Small city</td>\n<td>+0</td>\n</tr>\n<tr>\n<td>10,001&#8211;25,000</td>\n<td>Large city</td>\n<td>-1</td>\n</tr>\n<tr>\n<td>More than 25,000</td>\n<td>Metropolis</td>\n<td>-2</td>\n</tr>\n</table>\n<strong>Table: Miscellaneous Modifiers</strong><br>\n<table class=\"wiki-content-table\">\n<tr>\n<th>Factor</th>\n<th>Modifier</th>\n</tr>\n<tr>\n<td>Settlement is predominantly of a different alignment to you.</td>\n<td>+1 per step (cumulative)</td>\n</tr>\n<tr>\n<td>Population of settlement is at least 50% of a particular race you are seeking.</td>\n<td>-2</td>\n</tr>\n<tr>\n<td>5% to 25% of the population of the settlement is of a particular race you are seeking.</td>\n<td>+1</td>\n</tr>\n<tr>\n<td>Less than 5% of the population is of a particular race you are seeking.</td>\n<td>+2</td>\n</tr>\n<tr>\n<td>You are looking for your own race.</td>\n<td>-2</td>\n</tr>\n<tr>\n<td>You do not specify the profession.</td>\n<td>-2</td>\n</tr>\n<tr>\n<td>You do not specify tradition.</td>\n<td>-2</td>\n</tr>\n<tr>\n<td>You are attempting to recruit a contact.</td>\n<td>-5, ignore DC modifications for alignment, race, profession, and tradition.</td>\n</tr>\n<tr>\n<td>A cohort has been killed and not resurrected.</td>\n<td>+5 (cumulative for each death, reduced by 1 for each month since the cohort&#8217;s death)</td>\n</tr>\n<tr>\n<td>Signing bonus of 10 gp per Hit Die.</td>\n<td>-1 (cumulative, may be applied up to 5 times)</td>\n</tr>\n<tr>\n<td>Supplying magical equipment.</td>\n<td>Reduces DC equal to half the cumulative enhancement bonus of weapons and armor, (minimum reduction of -1).</td>\n</tr>\n<tr>\n<td>Settlement is particularly insular, isolated, or opposed to you.</td>\n<td>Varies, GM decision</td>\n</tr>\n</table>\n<h3 id=\"toc4\"><span>Building A Cohort</span></h3>\n<p>Cohorts gain abilities based on their Hit Dice and race. The listed statistics are modified by the cohort&#8217;s race as normal. Cohorts are considered to have a CR equal to their Hit Dice - 2 for purposes that require it. Cohorts have class skills according to their creature type (Climb, Craft, Handle Animal, Heal, Profession, Ride, and Survival for humanoids), plus the ones listed in their profession. Select from the following professions to determine the cohort&#8217;s base statistics:</p>\n<p><strong>Student</strong><br>\n<strong>Hit Dice</strong> d6; <strong>Saves</strong> Fort (bad), Ref (good), Will (good); <strong>Ability Scores</strong> Str 11, Dex 12, Con 13, Int 16, Wis 9, Cha 10; <strong>Practitioner Modifier</strong> Int; <strong>Skill Points per Hit Die</strong> 2; <strong>Class Skills</strong> Appraise, Knowledge (all), Use Magic Device; <strong>Magic Scholar:</strong> Students gain the Alchemy sphere as a bonus talent at 1 Hit Die. Students add half their level (minimum 1) to all Use Magic Device checks and may use their Intelligence in place of their Charisma for such checks.; <strong>Budding Academic:</strong> Students gain an additional 4 skill points per Hit Die that must be spent on Intelligence-based skills. <strong>Starting Equipment:</strong> Alchemy kit, simple melee or ranged weapon with 20 pieces ammunition</p>\n<p><strong>Thief</strong><br>\n<strong>Hit Dice</strong> d8; <strong>Saves</strong> Fort (bad), Ref (good), Will (good); <strong>Ability Scores</strong> Str 12, Dex 16, Con 13, Int 11, Wis 9, Cha 10; <strong>Practitioner Modifier</strong> Int; <strong>Skill Points per Hit Die</strong> 4; <strong>Class Skills</strong> Appraise, Disguise, Disable Device, Perception, Sleight of Hand, Stealth; <strong>Rogue Training:</strong> Thieves may take rogue talents in place of feats and gain the following ability: <em>Trapfinding:</em> A thief cohort adds 1/2 her level to Perception skill checks made to locate traps and to Disable Device skill checks (minimum +1). A thief cohort can use Disable Device to disarm magic traps. <strong>Starting Equipment:</strong> Studded leather armor, thieves tools, One of: 1. Simple or martial melee weapon and simple or martial ranged weapon with 20 pieces ammunition, or 2. Simple or martial melee weapon and shield.</p>\n<p><strong>Warrior</strong><br>\n<strong>Hit Dice</strong> d10; <strong>Saves</strong> Fort (good), Ref (bad), Will (good); <strong>Ability Scores</strong> Str 16, Dex 12, Con 13, Int 9, Wis 10, Cha 11; <strong>Practitioner Modifier</strong> Con; <strong>Skill Points per Hit Die</strong> 4; <strong>Class Skills</strong> Intimidate, Knowledge (dungeoneering), Knowledge (local), Knowledge (nobility), Perception, Swim; <strong>Athlete:</strong> Warrior cohorts gain a bonus equal to half their Hit Dice on all Climb and Swim skill checks as well as on Constitution-based ability checks. <strong>Starting Equipment:</strong> scale mail or studded leather armor, One of: 1. Simple or martial melee weapon and simple or martial ranged weapon with 20 pieces ammunition 2. Simple or martial melee weapon and shield</p>\n<p><strong>Woodsman</strong><br>\n<strong>Hit Dice</strong> d10; <strong>Saves</strong> Fort (good), Ref (good), Will (bad); <strong>Ability Scores</strong> Str 12, Dex 16, Con 13, Int 9, Wis 11, Cha 10; <strong>Practitioner Modifier</strong> Wis; <strong>Skill Points per Hit Die</strong> 4; <strong>Class Skills</strong> Knowledge (geography), Knowledge (nature), Perception, Sense Motive, Stealth, Swim; <strong>Survivalist:</strong> Woodsman cohorts gain a bonus equal to half their Hit Dice on all Survival skill checks. <strong>Starting Equipment</strong> Studded leather armor, a simple or martial melee weapon and simple or martial ranged weapon with 20 pieces ammunition</p>\n<p>When a cohort gains variable starting equipment (for example, a choice of weapons or shields) the total cost of the combined weapons, ammunition, and/or shield cannot exceed 100 gp.</p>\n<p>All cohorts are proficient with simple weapons, light armor, and bucklers. Cohorts begin play with leather armor and a dagger in addition to any equipment listed in their descriptions; further equipment must be supplied to them. A cohort&#8217;s starting gear may not be used to increase PC wealth; it cannot normally be sold and if extreme circumstances occur where the PCs benefit from its loss, it must be replaced (or wealth sufficient to pay for replacements given) at the earliest possible time. If the cohort is deceased, this payment must be made to their next of kin or other designee (it is assumed that such a person always exists as part of normal recruitment). If a cohort is equipped with additional items, the cohort will not keep them if leaving your service.</p>\n<strong>Table: Cohort</strong><br>\n<table class=\"wiki-content-table\">\n<tr>\n<th>Ranks in Diplomacy</th>\n<th>Maximum Hit Dice</th>\n<th>Base Attack Bonus</th>\n<th>Feats</th>\n<th>Good Saves</th>\n<th>Bad Saves</th>\n<th>Combat Talents</th>\n<th>Special</th>\n</tr>\n<tr>\n<td>1</td>\n<td>1</td>\n<td>+1</td>\n<td>1</td>\n<td>+2</td>\n<td>+0</td>\n<td>0</td>\n<td>Martial tradition</td>\n</tr>\n<tr>\n<td>2</td>\n<td>2</td>\n<td>+2</td>\n<td>1</td>\n<td>+3</td>\n<td>+0</td>\n<td>1</td>\n<td>-</td>\n</tr>\n<tr>\n<td>3</td>\n<td>3</td>\n<td>+3</td>\n<td>2</td>\n<td>+3</td>\n<td>+1</td>\n<td>1</td>\n<td>-</td>\n</tr>\n<tr>\n<td>4</td>\n<td>3</td>\n<td>+3</td>\n<td>2</td>\n<td>+3</td>\n<td>+1</td>\n<td>1</td>\n<td>-</td>\n</tr>\n<tr>\n<td>5</td>\n<td>4</td>\n<td>+4</td>\n<td>2</td>\n<td>+4</td>\n<td>+1</td>\n<td>2</td>\n<td>Ability score increase</td>\n</tr>\n<tr>\n<td>6</td>\n<td>5</td>\n<td>+5</td>\n<td>3</td>\n<td>+4</td>\n<td>+1</td>\n<td>2</td>\n<td>+2 enhancement</td>\n</tr>\n<tr>\n<td>7</td>\n<td>6</td>\n<td>+6/+1</td>\n<td>3</td>\n<td>+5</td>\n<td>+2</td>\n<td>3</td>\n<td>-</td>\n</tr>\n<tr>\n<td>8</td>\n<td>6</td>\n<td>+6/+1</td>\n<td>3</td>\n<td>+5</td>\n<td>+2</td>\n<td>3</td>\n<td>-</td>\n</tr>\n<tr>\n<td>9</td>\n<td>7</td>\n<td>+7/+2</td>\n<td>4</td>\n<td>+5</td>\n<td>+2</td>\n<td>3</td>\n<td>-</td>\n</tr>\n<tr>\n<td>10</td>\n<td>8</td>\n<td>+8/+3</td>\n<td>4</td>\n<td>+6</td>\n<td>+2</td>\n<td>4</td>\n<td>Ability score increase</td>\n</tr>\n<tr>\n<td>11</td>\n<td>9</td>\n<td>+9/+4</td>\n<td>5</td>\n<td>+6</td>\n<td>+3</td>\n<td>4</td>\n<td>+4/+2 enhancement</td>\n</tr>\n<tr>\n<td>12</td>\n<td>9</td>\n<td>+9/+4</td>\n<td>5</td>\n<td>+6</td>\n<td>+3</td>\n<td>4</td>\n<td>-</td>\n</tr>\n<tr>\n<td>13</td>\n<td>10</td>\n<td>+10/+5</td>\n<td>5</td>\n<td>+7</td>\n<td>+3</td>\n<td>5</td>\n<td>-</td>\n</tr>\n<tr>\n<td>14</td>\n<td>11</td>\n<td>+11/+6/+1</td>\n<td>6</td>\n<td>+7</td>\n<td>+3</td>\n<td>5</td>\n<td>-</td>\n</tr>\n<tr>\n<td>15</td>\n<td>12</td>\n<td>+12/+7/+2</td>\n<td>6</td>\n<td>+8</td>\n<td>+4</td>\n<td>6</td>\n<td>Ability score increase</td>\n</tr>\n<tr>\n<td>16</td>\n<td>12</td>\n<td>+12/+7/+2</td>\n<td>6</td>\n<td>+8</td>\n<td>+4</td>\n<td>6</td>\n<td>-</td>\n</tr>\n<tr>\n<td>17</td>\n<td>13</td>\n<td>+13/+8/+3</td>\n<td>7</td>\n<td>+8</td>\n<td>+4</td>\n<td>6</td>\n<td>+6/+4/+2 enhancement</td>\n</tr>\n<tr>\n<td>18</td>\n<td>14</td>\n<td>+14/+9/+4</td>\n<td>7</td>\n<td>+9</td>\n<td>+4</td>\n<td>7</td>\n<td>-</td>\n</tr>\n<tr>\n<td>19</td>\n<td>15</td>\n<td>+15/+10/+5</td>\n<td>8</td>\n<td>+9</td>\n<td>+5</td>\n<td>7</td>\n<td>-</td>\n</tr>\n<tr>\n<td>20</td>\n<td>15</td>\n<td>+15/+10/+5</td>\n<td>8</td>\n<td>+9</td>\n<td>+5</td>\n<td>7</td>\n<td>-</td>\n</tr>\n<tr>\n<td>21</td>\n<td>16</td>\n<td>+16/+11/+6/+1</td>\n<td>8</td>\n<td>+10</td>\n<td>+5</td>\n<td>8</td>\n<td>Ability score increase</td>\n</tr>\n</table>\n<p>When you gain a level and your ranks in Diplomacy increase, you may assign an additional Hit Die to current cohorts, up to the maximum they may possess and not causing the total Hit Dice you have recruited to exceed your limit.</p>\n<p><strong>Feats:</strong> A cohort begins with one feat, and gains another feat at every odd Hit Die. A cohort may gain any PC feat for which it qualifies. Cohorts may not gain magic item crafting feats.</p>\n<p><strong>Combat Talents:</strong> Cohorts gains talents with the proficient progression.</p>\n<p><strong>Martial Tradition:</strong> Your cohort gains a martial tradition, using the practitioner modifier given by their profession.</p>\n<p><strong>Ability Score Increase:</strong> Just like PCs and Monsters, a cohort gains a permanent +1 bonus to an ability score of your choice for every 4 Hit Dice possessed.</p>\n<p><strong>Enhancement:</strong> At 5 Hit Dice, a cohort gains a +2 enhancement bonus to one ability score. At 9th level, they increase this bonus to +4 and gain a +2 bonus to an additional ability score. At 13th level, the first bonus increases to +6, the second to +4, and they gain a +2 bonus to an additional ability score. These bonuses may be retrained as if they were a single feat.</p>\n<p>Talents marked (cohort) grant additional options for your cohorts. Some abilities call out joint actions. These actions require both the user and his cohort to pay the requisite action cost.</p>\n<h3 id=\"toc5\"><span>Re-Recruiting Cohorts</span></h3>\n<p>A cohort that leaves your service on good terms is easier to recruit. You may maintain a network of such former cohorts, called contacts, equal to twice your practitioner ability modifier (minimum 2). Such contacts do not normally travel with you beyond reaching the nearest settlement. Each contact will have a place of residence, usually in a settlement but sometimes a wilderness area, ship, or other abode. You may send messages to a contact by whatever means you have available, requesting that they meet you at a particular time and place, allowing you to recruit them. Talents such a Messenger, and legendary talents such as Air Travel, Planisphere, and Teleportation can facilitate getting messages to the contact and their traveling to meet you. Contacts are able to travel to your caravan at the same rate as your Messengers.</p>\n<p>Contacts have lives apart from following you and may on occasion send requests for aid or show up in unexpected places. Unlike a cohort, there is no penalty for the death of a contact. Mindless cohorts may not become contacts.</p>\n<h3 id=\"toc6\"><span>Roleplaying Cohorts</span></h3>\n<p>Cohorts are generally under the player's control regarding their mechanical actions, especially during adventures, but they are not mindless automatons. Cohorts that are abused or mistreated will abandon the player when able to do so. Normally, the GM will speak for cohorts. Another option is to allow other players to control some cohorts, investing them in the cohort's development and well-being. The other players could be allowed a degree of latitude to portray a unique personality for the cohort, emphasizing that it is a unique person rather than simply a class ability. This should obviously only be done after consulting with the other players and may not work in all groups. Be aware that this invites the others players' input on how the cohort is used, which may be unwelcome to some players.</p>\n<h2 id=\"toc7\"><span>Follower Package</span></h2>\n<p>You gain a group of followers. These followers will not accompany you into dangerous locations, but can lend assistance in various ways. Followers can supply unskilled labor for simple tasks, such as clearing a road obstructed by falling rocks. Count the followers as supplying a number of Medium-sized unskilled laborers equal to half the number of followers in the settlement (or caravan). While doing so, the followers cannot provide their other benefits. If attacked, followers will attempt to flee to a safe place to the best of their ability. You normally have about 5 followers per rank in Diplomacy. If statistics are needed for a particular follower, treat them as level 1 commoners.</p>\n<p>If you possess the Alchemists, Craftsmen, Entertainers, Healers, Merchants, Rangers, or Scholars talents, then there is a 10% chance per talent that a randomly selected follower will be a level 1 expert. When you gain this package, choose a settlement with which you are well acquainted. Your initial followers are drawn from there. When first gaining this package, it is assumed that you have spent time in the past making contacts, deals, and promises and that you are well known enough in the community to attract a group of followers.</p>\n<p>Should the number of followers available be reduced for any reason, the effectiveness of (followers) talents is reduced, treating your ranks in Diplomacy as equal to the number of available followers/4. You normally regain 1 follower per rank in Diplomacy each day you spend in a settlement where your followers are dispersed, but you can double this number with 8 hours of active effort each day. If you have lost all of your followers or do not disperse your followers in the settlement, the recruitment rate is halved (minimum 1 follower per day).</p>\n<p>If using the downtime rules found in Ultimate Campaign, having your followers present in a settlement increases the effect of Influence or Labor you spend by 50%, to a maximum of 1 additional Influence or Labor for every 2 followers in the settlement where the downtime activity takes place. Any talents that enable followers to make checks to generate capital receive any benefits from rooms and teams you have access to. If a talent allows followers to generate capital, the entire group of followers makes this check collectively. Any results that are not sufficient to generate capital on a given day may be carried over to the next day; this carrying-over may be continued until the result is sufficient to generate capital or your followers leave the settlement.</p>\n<p>If using the kingdom building rules found in Ultimate Campaign, possessing this package grants a +1 bonus to all kingdom attributes you affect.</p>\n<p>When you first gain the follower package, you gain the following ability:</p>\n",
      "unidentified": ""
    },
    "tags": [],
    "actions": [],
    "uses": {
      "per": null,
      "value": 0,
      "maxFormula": "",
      "autoDeductChargesCost": "1"
    },
    "attackNotes": [],
    "effectNotes": [],
    "changes": [],
    "changeFlags": {
      "loseDexToAC": false,
      "noEncumbrance": false,
      "mediumArmorFullSpeed": false,
      "heavyArmorFullSpeed": false
    },
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "tag": "",
    "useCustomTag": false,
    "armorProf": {
      "value": [],
      "custom": ""
    },
    "weaponProf": {
      "value": [],
      "custom": ""
    },
    "languages": {
      "value": [],
      "custom": ""
    },
    "flags": {
      "boolean": {},
      "dictionary": {}
    },
    "scriptCalls": [],
    "subType": "combatTalent",
    "associations": {
      "classes": []
    },
    "showInQuickbar": false,
    "abilityType": "none",
    "crOffset": "",
    "disabled": false,
    "classSkills": {},
    "nonlethal": false,
    "duration": {
      "value": null,
      "units": ""
    },
    "target": {
      "value": ""
    },
    "attackName": "",
    "actionType": null,
    "attackBonus": "",
    "critConfirmBonus": "",
    "damage": {
      "parts": [],
      "critParts": [],
      "nonCritParts": []
    },
    "attackParts": [],
    "formulaicAttacks": {
      "count": {
        "formula": ""
      },
      "bonus": {
        "formula": ""
      },
      "label": null
    },
    "formula": "",
    "ability": {
      "attack": null,
      "damage": null,
      "damageMult": 1,
      "critRange": 20,
      "critMult": 2
    },
    "save": {
      "dc": 0,
      "type": "",
      "description": ""
    },
    "soundEffect": "",
    "powerAttack": {
      "multiplier": "",
      "damageBonus": 2,
      "critMultiplier": 1
    }
  },
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "9.0",
    "coreVersion": "10.291",
    "createdTime": 1663168888173,
    "modifiedTime": 1689623921486,
    "lastModifiedBy": "pf1spheres-db000"
  }
}
