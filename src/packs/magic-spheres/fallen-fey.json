{
  "_id": "3QDmfBtK20lr0Ohu",
  "name": "Fallen Fey",
  "folder": null,
  "sort": 0,
  "flags": {
    "pf1spheres": {
      "sphere": "fallenFey"
    }
  },
  "pages": [
    {
      "name": "Fallen Fey",
      "type": "text",
      "title": {
        "show": false,
        "level": 1
      },
      "text": {
        "format": 1,
        "content": "<img src=\"modules/pf1spheres/assets/icons/spheres/fallen_fey.webp\" style=\"float:right;border:none;outline:none\" /><h3 id=\"toc0\"><span>Note: Flavor and Faerie</span></h3>\n<p>The Fallen Fey sphere is unusual in Spheres of Power because it has a much stronger flavor/theme when compared to other, largely-generic spheres. However, if the fey focus of this sphere does not appeal to you, you can plausibly change it to represent almost any other group by renaming talents and changing the 'image' of each one. For example, you can change Animate Hair to animated light, moving cogs, or fiendish chains while retaining the same mechanical effects. A little creativity goes a long ways. -SoP Wiki Admin</p>\n<hr />\n<p>In its original publication in Player’s Guide to Skybourne, the Fallen Fey sphere was limited to elves (and, with GM permission, other fey-related races). While a GM is well within their rights to limit this sphere in a similar fashion depending upon the role of fey within their setting, no such limitation is included here.</p>\n<h2 id=\"toc1\"><span>Fey-Link</span></h2>\n<p>As a swift action, you change your creature type to fey for 1 minute per caster level. You are treated as a fey and no longer count as your previous creature type for all purposes including spells, magic items, etc., but you do not gain any of the benefits of the fey type or lose the benefits of your previous creature type, and still possess any subtypes you previously possessed (thus, an elf using this ability would be treated as a fey with the elf subtype).</p>\n<p>Whenever fey-link is active, you may spend 1 spell point as a free action to gain the benefits of a fey-blessing until the end of the fey-link. There is no limit to the number of fey-blessings you may have active at a time, but each one must be activated separately, and all only endure until the end of the fey-link. Renewing a fey-link does not increase the duration of a fey-blessing, and ends all fey-blessings you currently possess.</p>\n<p>When you gain the Fallen Fey sphere, you gain the following fey-blessing:</p>\n<h4 id=\"toc2\"><span>Nature-Connection (fey-blessing)</span></h4>\n<p>You gain a +1 bonus on initiative checks and Knowledge (geography), Survival, Stealth, and Perception checks made within one terrain of your choice (chosen when this fey-blessing is cast). This bonus increases by +1 for every 5 caster levels you possess. Consult the ranger list of favored terrains to see potential terrain types.</p>\n<hr />\n<h3 id=\"toc3\"><span>Rule Notes</span></h3>\n<p><strong>Alteration Sphere:</strong> Some fey-blessings belong to the polymorph subschool. A willing creature under the effects of one of these abilities may still be targeted by the shapeshift ability of the Alteration sphere or vice-versa. Each such fey-blessing maintained during the shapeshift reduces the number of traits that may be assigned as part of the shapeshift by 1. Unthreatening Form functions as a base transformation and may have traits added to it as if it were Blank Transformation.</p>\n<p><strong>Fey creatures and Fey-Blessings:</strong> Creatures with the fey type are unaffected by the base function of fey-link, but its duration is still needed for determining the length of time fey-blessings remain active. Due to their inherent connection, creatures of the fey type (naturally, not via fey-link) may begin a fey-link on themselves (and only themselves) as a free action.</p>\n<p><strong>Fey-Blessing with Differing Caster Levels:</strong> Fey-link and the fey-blessings applied to it do not necessarily have to have the same caster level. The duration and any other parameters based on the caster level of the fey-link are determined by the caster level with which it was cast. Likewise, any variable parameters of each individual fey-blessing are determined by the individual casting.</p>\n<h3 id=\"toc4\"><span>Fallen Fey Talent Types</span></h3>\n<p>Some talents are marked (fey-blessing). These talents grant additional fey-blessings.</p>\n<hr /><br />\n<br /><b>Fallen Fey Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.magic-talents.Ta4ltpBogv1emWjd]{Greater Fey-Link}</li>\n<li>@Compendium[pf1spheres.magic-talents.mcgrnrP6nsRT9lfd]{Share Link}</li>\n</ul><br /><b>Fey-blessing Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.magic-talents.PU5Z5kw8CtS3Z2iW]{Aelfwine}</li>\n<li>@Compendium[pf1spheres.magic-talents.dYOX2ka1BR2ywBsV]{Animate Hair}</li>\n<li>@Compendium[pf1spheres.magic-talents.JhddjFNpIOhEyxQ8]{Beastward}</li>\n<li>@Compendium[pf1spheres.magic-talents.AoCe29hAeAZwcRcV]{Beckoning Call}</li>\n<li>@Compendium[pf1spheres.magic-talents.s4LiCDLbHiOucpzT]{Crown Of The Courts}</li>\n<li>@Compendium[pf1spheres.magic-talents.xI1fRT6XfleJ7uGG]{Enchanting Music}</li>\n<li>@Compendium[pf1spheres.magic-talents.nPezUlZluCgfN2rh]{Fade}</li>\n<li>@Compendium[pf1spheres.magic-talents.nZ6PAKRCyPA0OHn3]{Fairy Dust}</li>\n<li>@Compendium[pf1spheres.magic-talents.c2j6ZRjDzmRrYi28]{Fairy Flight}</li>\n<li>@Compendium[pf1spheres.magic-talents.L9gJdXK2Zmw1xBDe]{Fae Light}</li>\n<li>@Compendium[pf1spheres.magic-talents.IqFeXFA8RXoCd5D5]{Feast And Famine}</li>\n<li>@Compendium[pf1spheres.magic-talents.sE2GcD3a7RJc5Ahy]{Feline Omens}</li>\n<li>Grimalkin Shade</li>\n<li>@Compendium[pf1spheres.magic-talents.3Ka8fs2cnVqco5KC]{Fey Beauty}</li>\n<li>@Compendium[pf1spheres.magic-talents.kq2ngY9TZaAlqs9r]{Fey Potency}</li>\n<li>@Compendium[pf1spheres.magic-talents.S1SHiuVLoubj4t38]{Fey Secrets}</li>\n<li>@Compendium[pf1spheres.magic-talents.aUg631y2rrOSccDm]{Grace Of The Sidhe}</li>\n<li>@Compendium[pf1spheres.magic-talents.o2oXwxlDRwopVevS]{Gremlin’s Presence}</li>\n<li>@Compendium[pf1spheres.magic-talents.gxA6a9L7lElTHHJk]{Listen To The Wind}</li>\n<li>@Compendium[pf1spheres.magic-talents.fvb1BiZUcqkqzvE4]{Long Step}</li>\n<li>@Compendium[pf1spheres.magic-talents.f0NvHLegxkIgJZ4L]{Mantle Of Autumn}</li>\n<li>@Compendium[pf1spheres.magic-talents.zj41t3l4NQrnOmMA]{Mantle Of Spring}</li>\n<li>@Compendium[pf1spheres.magic-talents.gSAS0gAsyx0Pco6E]{Misplacement}</li>\n<li>@Compendium[pf1spheres.magic-talents.s70A72h8EJHqB70B]{Natural Blessing}</li>\n<li>@Compendium[pf1spheres.magic-talents.5HQaMGd4GoCSZYVw]{Natural Dominion}</li>\n<li>@Compendium[pf1spheres.magic-talents.e4pN3903MnApkOwU]{Nature’s Empathy}</li>\n<li>@Compendium[pf1spheres.magic-talents.QIEW4SZZStrKDF0F]{Pixie Sticks}</li>\n<li>@Compendium[pf1spheres.magic-talents.l6OZu7nDsLK6nTPE]{Plant Friend}</li>\n<li>@Compendium[pf1spheres.magic-talents.ypddW6RWsGgwyaye]{Saboteur}</li>\n<li>@Compendium[pf1spheres.magic-talents.9rxkIpKvWKQiZwLh]{Shadow Collector}</li>\n<li>@Compendium[pf1spheres.magic-talents.Kio30f5nXNiGoqkQ]{Snare Setter}</li>\n<li>@Compendium[pf1spheres.magic-talents.WqpKAZMRRWvjyhBs]{Spores}</li>\n<li>@Compendium[pf1spheres.magic-talents.ye31l21Zc5ofibcx]{Stone Shape}</li>\n<li>@Compendium[pf1spheres.magic-talents.Kq0iyjGMOPC6EYHb]{Stunning Glance}</li>\n<li>@Compendium[pf1spheres.magic-talents.I3gLEej6XD3cQ3h0]{Summon Fairy}</li>\n<li>@Compendium[pf1spheres.magic-talents.uP4sNlviz35fNAjp]{Tree Meld}</li>\n<li>@Compendium[pf1spheres.magic-talents.ICPCNY9VDEaDNCA9]{Trickery}</li>\n<li>@Compendium[pf1spheres.magic-talents.eFX0hNkLS4kUAJQ6]{Unseelie Aura}</li>\n<li>@Compendium[pf1spheres.magic-talents.griXAioSVehagV2N]{Unthreatening Form}</li>\n<li>@Compendium[pf1spheres.magic-talents.dJTFsT4MZXXMrOaT]{Ventriloquism}</li>\n<li>@Compendium[pf1spheres.magic-talents.low7kkPcTTW6TCUS]{Water Creature}</li>\n<li>@Compendium[pf1spheres.magic-talents.nVsIp4Vnv3FatXhG]{Weapons Of The Wild}</li>\n<li>@Compendium[pf1spheres.magic-talents.uP3NHUfanPUjqMTQ]{Wild Walk}</li>\n<li>@Compendium[pf1spheres.magic-talents.6btLOSu5yBBlGZhi]{Winterfey}</li>\n<li>@Compendium[pf1spheres.magic-talents.uV4PleJZi7tg0BhH]{Wood Shape}</li>\n<li>@Compendium[pf1spheres.magic-talents.RBicPqFhCcyGQk4G]{Zolavoi’s Mantle}</li>\n</ul><br /><b>Advanced Fallen Fey Talents</b><br /><ul>\n<li>@Compendium[pf1spheres.magic-talents.hxhIMzbdK5CHnAga]{Banish to Faerie}</li>\n<li>@Compendium[pf1spheres.magic-talents.dwMT5XbOuc98rd7J]{Blinding Beauty}</li>\n<li>@Compendium[pf1spheres.magic-talents.6vO58PX4f8Ap5U3W]{Bound To Nature}</li>\n<li>@Compendium[pf1spheres.magic-talents.iG9qW7Xb2Dm6B3N4]{Drowning Kiss}</li>\n<li>@Compendium[pf1spheres.magic-talents.abY7Mc7ZPuhN48p5]{Fairy Ring Traveler}</li>\n<li>@Compendium[pf1spheres.magic-talents.DdLVlM53ZmecZDCM]{Fey Initiation}</li>\n<li>@Compendium[pf1spheres.magic-talents.J25bmMFTQZHlz5M5]{Fey Invisibility}</li>\n<li>@Compendium[pf1spheres.magic-talents.u0PaqjiII6GrbzFr]{Steal Skin}</li></ul>"
      },
      "_id": "FTIK9381ihV7qLU0",
      "image": {},
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "sort": 0,
      "ownership": {
        "default": -1
      },
      "flags": {}
    }
  ],
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "0.82.2",
    "coreVersion": "10.285",
    "createdTime": 1663169295249,
    "modifiedTime": 1663181741358,
    "lastModifiedBy": "pf1spheres-db000"
  }
}
