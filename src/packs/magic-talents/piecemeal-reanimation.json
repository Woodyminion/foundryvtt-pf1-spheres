{
  "_id": "0T8s9NlECusmNejN",
  "name": "Piecemeal Reanimation",
  "type": "feat",
  "img": "modules/pf1spheres/assets/icons/spheres/death.webp",
  "effects": [],
  "folder": null,
  "sort": 0,
  "flags": {
    "pf1spheres": {
      "sphere": "death"
    }
  },
  "system": {
    "description": {
      "value": "<p>You gain the following abilities which allow you to reanimate incomplete corpses and individual corpse parts:</p>\n<p><strong>Reanimate Piecemeal Undead:</strong> You can reanimate an individual part or parts of a corpse as a piecemeal undead, such as reanimating a corpse&#8217;s arm or skull or a torso with an arm. When reanimating piecemeal undead this way, use the statistics of an appropriately sized animated object (see the Enhancement sphere, Ultimate Spheres of Power pg. 288, 577) with the undead construction flaw and no other construction point abilities or construction flaws. Piecemeal undead otherwise count as an undead creature you reanimated, count against the total number of reanimated creatures you may have active at any one time normally, and obey your commands as normal for an undead you reanimated. A piecemeal undead will usually be anywhere from three sizes smaller to approximately the same size as the original corpse. See Table: Piecemeal Undead to determine the appropriate animated object size for a piecemeal undead.</p>\n<p>A piecemeal undead is not a skeleton or zombie and cannot be modified by the Expanded Necromancy talent, except as described below. Piecemeal undead do not count as animated objects for the purposes of Enhancement sphere talents, feats, and other abilities.</p>\n<p><strong>Infuse Piecemeal Undead:</strong> You can grant an intact undead under your control the ability to split off parts of itself to create piecemeal undead as described in the Reanimate Piecemeal Undead ability. As a standard action, or as a free action when reanimating one or more undead, you may spend a spell point to grant one undead within your dominion range the ability to split off a part of itself. If you possess Mass Death Magic, you may spend an additional spell point to grant this ability to 1 additional undead per 2 caster levels (minimum 1). An undead granted the ability to split off a part of itself can spend a full-round action to create a piecemeal undead of an appropriate size to the body part used into an adjacent, unoccupied space (such as a zombie tearing off its own arm or hand, or the skull of a skeleton popping off). You must be able to control the Hit Dice of a piecemeal undead created this way or it fails to be created. Piecemeal undead created this way share the duration of the reanimate effect which initially created the intact undead it was separated from and possesses any properties (including defensive abilities, special abilities, and special attacks) possessed by that undead, subject to GM discretion, but does not increase any existing defensive abilities it would possess as a piecemeal undead (such as damage reduction or natural armor). This can include abilities granted by a variant, such as those from the Expanded Necromancy talent, allowing for a bloody skeleton to separate its arm from itself to create an appropriately sized &#8220;bloody&#8221; piecemeal undead with the fast healing and deathless qualities from the bloody skeleton variant.</p>\n<p>Only undead raised from intact corpses, such as those normally available without this talent, can be granted the ability to split with this talent; piecemeal undead can never be granted the ability to split off parts of themselves. An intact undead can only separate a single part of itself as a piecemeal undead at any given time, but can spend a move action when within reach of the piecemeal undead created from its body to rejoin with it. When an undead separates a part of itself to create a piecemeal undead, it loses a number of hit points equal to the piecemeal undead&#8217;s hit points, to a maximum of half the intact undead&#8217;s current hit points. If the body part used grants the intact undead a natural attack (such as a skeleton removing its arm), that undead loses that natural attack until it rejoins with the piecemeal undead. If the body part is one of the intact undead&#8217;s legs or wings, it reduces its movement speed with the appropriate movement type by 10 feet until it rejoins with the piecemeal undead.</p>\n<strong>Table: Piecemeal Undead</strong><br>\n<table class=\"wiki-content-table\">\n<tr>\n<th>Piecemeal Undead Size</th>\n<th>Body Part Example</th>\n</tr>\n<tr>\n<td>Three sizes smaller than corpse</td>\n<td>Finger, toe, other minor body</td>\n</tr>\n<tr>\n<td>Two sizes smaller than corpse</td>\n<td>Foot, hand, organ</td>\n</tr>\n<tr>\n<td>One size smaller than corpse</td>\n<td>Arm, leg, skull</td>\n</tr>\n<tr>\n<td>Same size as corpse</td>\n<td>Torso plus limb(s)</td>\n</tr>\n</table>\n",
      "unidentified": ""
    },
    "tags": ["Dominion"],
    "actions": [],
    "uses": {
      "per": null,
      "value": 0,
      "maxFormula": "",
      "autoDeductChargesCost": "1"
    },
    "attackNotes": [],
    "effectNotes": [],
    "changes": [],
    "changeFlags": {
      "loseDexToAC": false,
      "noEncumbrance": false,
      "mediumArmorFullSpeed": false,
      "heavyArmorFullSpeed": false
    },
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "tag": "",
    "useCustomTag": false,
    "armorProf": {
      "value": [],
      "custom": ""
    },
    "weaponProf": {
      "value": [],
      "custom": ""
    },
    "languages": {
      "value": [],
      "custom": ""
    },
    "flags": {
      "boolean": {},
      "dictionary": {}
    },
    "scriptCalls": [],
    "subType": "magicTalent",
    "associations": {
      "classes": []
    },
    "showInQuickbar": false,
    "abilityType": "none",
    "crOffset": "",
    "disabled": false,
    "classSkills": {},
    "nonlethal": false,
    "duration": {
      "value": null,
      "units": ""
    },
    "target": {
      "value": ""
    },
    "attackName": "",
    "actionType": null,
    "attackBonus": "",
    "critConfirmBonus": "",
    "damage": {
      "parts": [],
      "critParts": [],
      "nonCritParts": []
    },
    "attackParts": [],
    "formulaicAttacks": {
      "count": {
        "formula": ""
      },
      "bonus": {
        "formula": ""
      },
      "label": null
    },
    "formula": "",
    "ability": {
      "attack": null,
      "damage": null,
      "damageMult": 1,
      "critRange": 20,
      "critMult": 2
    },
    "save": {
      "dc": 0,
      "type": "",
      "description": ""
    },
    "soundEffect": "",
    "powerAttack": {
      "multiplier": "",
      "damageBonus": 2,
      "critMultiplier": 1
    }
  },
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "9.0",
    "coreVersion": "10.291",
    "createdTime": 1663168890395,
    "modifiedTime": 1689623922691,
    "lastModifiedBy": "pf1spheres-db000"
  }
}
