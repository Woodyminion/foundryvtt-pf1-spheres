{
  "_id": "dnurcG25r4SDbCj2",
  "name": "Repair",
  "type": "feat",
  "img": "modules/pf1spheres/assets/icons/spheres/creation.webp",
  "effects": [],
  "folder": null,
  "sort": 0,
  "flags": {
    "pf1spheres": {
      "sphere": "creation"
    }
  },
  "system": {
    "description": {
      "value": "<p>You may repair a damaged object, healing it for a number of hit points equal to 1d4 + 1/2 your caster level. If the object has the broken condition, this condition is removed if the object is restored to at least half its original hit points.</p>\n<p>This ability cannot restore warped or transmuted items, but it can still repair damage done to such items.</p>\n<h2 id=\"toc3\"><span>Create</span></h2>\n<p>As a standard action, you may spend a spell point to create a non-magical, unattended object out of vegetable matter such as wood, hemp, or cotton in either your hand or an adjacent square. You may create objects with multiple materials, provided you can create all the materials required. Adamantine cannot be altered or created, except for the Repair and Destroy abilities. The object may be of equivalent size to one Small object per caster level (see chart below) and lasts as long as you continue to concentrate, to a maximum of 1 minute per caster level. When creating an object you may spend an additional spell point to allow that object to persist for 1 minute per caster level without concentration. A created object may be dismissed as a standard action.</p>\n<p>If the created object is especially large, it must begin in an adjacent square and must be completely contained within close range. You may choose to create objects that can be moved easily, or can create objects that are anchored to an unattended surface large enough to hold that part of the object (such as a bridge being anchored to both sides of a gap, or a wall growing out of the ground). You cannot create an object directly onto a creature or attended object (summoning manacles onto someone&#8217;s wrists, etc.).</p>\n<p>When attempting to break an object that does not have a predetermined Break DC, the Break DC is equal to 10 + the object&#8217;s hardness +1 per inch in the item&#8217;s thickness. For example, a 2-inch thick stone wall would have a Break DC of 10 + 8 hardness + 2 inches, or DC 20.</p>\n<p>Generally speaking, you cannot create alchemical items, poisons, materials that deal damage on contact or items that carry special properties or knowledge you do not possess (rare herbs, the key to a lock you did not construct, etc.). While simple objects such as candles, folds of cloth, simple furniture, or basic weapons are easy to create; particularly complex objects (mechanics, crossbows, objects with moving parts) require a Craft check made against the object&#8217;s Craft DC. Likewise, non-magical common herbs can be created through a successful Knowledge (Nature) check, such as wolfsbane or the night tea plant. For herbs and items with no predetermined Craft DC, the check DC is assumed to be equal to 10 +1 per 5 gp in the item&#8217;s cost, though any items with a DC above 15 cannot be made. Failure means the object comes into being broken and unusable, or otherwise incorrect.</p>\n<p>A DC 15 Appraise check reveals the object as a magical fake. Fabricated objects have a lingering magical aura that can be detected as magic, although the objects themselves are not magical.</p>\n<strong>Table: Object Materials</strong><br>\n<table class=\"wiki-content-table\">\n<tr>\n<th>Substance</th>\n<th>Hardness</th>\n<th>Hit Points</th>\n</tr>\n<tr>\n<td>Glass</td>\n<td>1</td>\n<td>1/in. of thickness</td>\n</tr>\n<tr>\n<td>Paper or cloth</td>\n<td>0</td>\n<td>2/in. of thickness</td>\n</tr>\n<tr>\n<td>Rope</td>\n<td>0</td>\n<td>2/in. of thickness</td>\n</tr>\n<tr>\n<td>Ice</td>\n<td>0</td>\n<td>3/in. of thickness</td>\n</tr>\n<tr>\n<td>Leather or hide</td>\n<td>2</td>\n<td>5/in. of thickness</td>\n</tr>\n<tr>\n<td>Wood</td>\n<td>5</td>\n<td>10/in. of thickness</td>\n</tr>\n<tr>\n<td>Stone</td>\n<td>8</td>\n<td>15/in. of thickness</td>\n</tr>\n<tr>\n<td>Iron or steel</td>\n<td>10</td>\n<td>30/in. of thickness</td>\n</tr>\n<tr>\n<td>Mithral</td>\n<td>15</td>\n<td>30/in. of thickness</td>\n</tr>\n<tr>\n<td>Adamantine</td>\n<td>20</td>\n<td>40/in. of thickness</td>\n</tr>\n</table>\n<strong>Table: Object Size</strong><br>\n<table class=\"wiki-content-table\">\n<tr>\n<th>Object Size</th>\n<th>Minimum Caster level (number of Small objects contained)</th>\n<th>Example Objects</th>\n<th>Maximum Weight (lbs.)</th>\n<th>Falling Damage (to both object and target)</th>\n</tr>\n<tr>\n<td>Small</td>\n<td>1</td>\n<td>Chair</td>\n<td>25</td>\n<td>1d6</td>\n</tr>\n<tr>\n<td>Medium</td>\n<td>2</td>\n<td>Table</td>\n<td>125</td>\n<td>1d8</td>\n</tr>\n<tr>\n<td>Large</td>\n<td>4</td>\n<td>Statue</td>\n<td>625</td>\n<td>2d6</td>\n</tr>\n<tr>\n<td>Huge</td>\n<td>8</td>\n<td>Wagon</td>\n<td>3,125</td>\n<td>3d6</td>\n</tr>\n<tr>\n<td>Gargantuan</td>\n<td>16</td>\n<td>Catapult</td>\n<td>15,625</td>\n<td>4d6</td>\n</tr>\n<tr>\n<td>Colossal</td>\n<td>32</td>\n<td>Ship</td>\n<td>78,125</td>\n<td>5d6</td>\n</tr>\n<tr>\n<td>Colossal+</td>\n<td>64</td>\n<td>Tavern</td>\n<td>390,625</td>\n<td>6d6</td>\n</tr>\n</table>\n<p><sup><strong>Author's Note:</strong> The maximum weight table is intended to help when approximating created objects by weight and not volume (number of Small objects).</sup></p>\n<strong>Table: Weapon Sizes</strong><br>\n<table class=\"wiki-content-table\">\n<tr>\n<th>Weapon Type</th>\n<th>Size Adjustment (From Size of Intended Wielder)</th>\n</tr>\n<tr>\n<td>Shuriken, needles, etc.</td>\n<td>Four sizes smaller</td>\n</tr>\n<tr>\n<td>Ammunition, daggers</td>\n<td>Three sizes smaller</td>\n</tr>\n<tr>\n<td>Light weapons</td>\n<td>Two sizes smaller</td>\n</tr>\n<tr>\n<td>One-handed weapons</td>\n<td>One size smaller</td>\n</tr>\n<tr>\n<td>Two-handed weapons</td>\n<td>Same size</td>\n</tr>\n</table>\n<h3 id=\"toc4\"><span>Clarifications</span></h3>\n<p>There are dozens of ways a created object can interact with the world, and many of the details of those interactions are determined by the GM. The following notes, however, include clarifications about various types of created objects.</p>\n",
      "unidentified": ""
    },
    "tags": [],
    "actions": [],
    "uses": {
      "per": null,
      "value": 0,
      "maxFormula": "",
      "autoDeductChargesCost": "1"
    },
    "attackNotes": [],
    "effectNotes": [],
    "changes": [],
    "changeFlags": {
      "loseDexToAC": false,
      "noEncumbrance": false,
      "mediumArmorFullSpeed": false,
      "heavyArmorFullSpeed": false
    },
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "tag": "",
    "useCustomTag": false,
    "armorProf": {
      "value": [],
      "custom": ""
    },
    "weaponProf": {
      "value": [],
      "custom": ""
    },
    "languages": {
      "value": [],
      "custom": ""
    },
    "flags": {
      "boolean": {},
      "dictionary": {}
    },
    "scriptCalls": [],
    "subType": "magicTalent",
    "associations": {
      "classes": []
    },
    "showInQuickbar": false,
    "abilityType": "none",
    "crOffset": "",
    "disabled": false,
    "classSkills": {},
    "nonlethal": false,
    "duration": {
      "value": null,
      "units": ""
    },
    "target": {
      "value": ""
    },
    "attackName": "",
    "actionType": null,
    "attackBonus": "",
    "critConfirmBonus": "",
    "damage": {
      "parts": [],
      "critParts": [],
      "nonCritParts": []
    },
    "attackParts": [],
    "formulaicAttacks": {
      "count": {
        "formula": ""
      },
      "bonus": {
        "formula": ""
      },
      "label": null
    },
    "formula": "",
    "ability": {
      "attack": null,
      "damage": null,
      "damageMult": 1,
      "critRange": 20,
      "critMult": 2
    },
    "save": {
      "dc": 0,
      "type": "",
      "description": ""
    },
    "soundEffect": "",
    "powerAttack": {
      "multiplier": "",
      "damageBonus": 2,
      "critMultiplier": 1
    }
  },
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "9.0",
    "coreVersion": "10.291",
    "createdTime": 1663168892747,
    "modifiedTime": 1689623923402,
    "lastModifiedBy": "pf1spheres-db000"
  }
}
