{
  "_id": "67VjRJ3t454qgQCx",
  "name": "Summon",
  "type": "feat",
  "img": "modules/pf1spheres/assets/icons/spheres/conjuration.webp",
  "effects": [],
  "folder": null,
  "sort": 0,
  "flags": {
    "pf1spheres": {
      "sphere": "conjuration"
    }
  },
  "system": {
    "description": {
      "value": "<p>As a standard action, you may spend a spell point to summon a creature you have made a contract with (called a companion) causing it to appear in an adjacent square, ready to act on your following turn. You must concentrate to maintain the companion&#8217;s presence, but may always spend an additional spell point to allow the summoned creature to remain for 1 minute per caster level without concentration.</p>\n<p>Companions can take many forms; a caster could contract with sympathetic angels or demons, elemental spirits, or primordial beings only given form after the contract is made. Thus, a companion could have the form of a knight in armor, a demonic dog, a flying anthropomorphic cat, or indeed virtually any other form. You cannot choose a companion with the exact same appearance as another creature.</p>\n<p>When you gain the Conjuration sphere, you automatically gain a single companion of your choice. If a companion is conjured multiple times during a day, they do not regain hit points or other resources spent. If a companion is reduced to 0 hit points, they instantly disappear and cannot be summoned until the following day.</p>\n<p>A companion only recovers hit points and resources (unless restored through the Heal skill or magical means) when the caster rests to recover spell points. A companion may be dismissed as a free action.</p>\n<p>Companions may not carry equipment or items back and forth when summoned and so cannot be used to store items in their home plane or bring items to the caster&#8217;s plane. The exception to this rule is equipment gained through Conjuration talents, which automatically accompany the companion. Whenever you gain a companion, they immediately gain 1 (form) or (type) talent of your choice.</p>\n<p>Every companion comes with one of the following base forms, chosen by the caster. Once made, this choice cannot be altered. Companions also gain power as their caster&#8217;s power grows, according to Table: Companion.</p>\n<p>(Note: A companion does not gain power from temporary increases to caster level; i.e., from a thaumaturge&#8217;s forbidden lore ability or from certain boons.)</p>\n<strong>Table: Companion</strong><br>\n<table class=\"wiki-content-table\">\n<tr>\n<th>Caster&#8217;s Caster Level</th>\n<th>Hit Dice</th>\n<th>Base Attack Bonus</th>\n<th>Skill Points</th>\n<th>Feats</th>\n<th>Natural Armor Bonus</th>\n<th>Good Saves</th>\n<th>Bad Saves</th>\n<th>Special</th>\n</tr>\n<tr>\n<td>1st</td>\n<td>1</td>\n<td>+1</td>\n<td>1</td>\n<td>1</td>\n<td>+0</td>\n<td>+2</td>\n<td>+0</td>\n<td>-</td>\n</tr>\n<tr>\n<td>2nd</td>\n<td>2</td>\n<td>+2</td>\n<td>2</td>\n<td>1</td>\n<td>+1</td>\n<td>+3</td>\n<td>+0</td>\n<td>Evasion</td>\n</tr>\n<tr>\n<td>3rd</td>\n<td>3</td>\n<td>+3</td>\n<td>3</td>\n<td>2</td>\n<td>+1</td>\n<td>+3</td>\n<td>+1</td>\n<td>-</td>\n</tr>\n<tr>\n<td>4th</td>\n<td>3</td>\n<td>+3</td>\n<td>3</td>\n<td>2</td>\n<td>+1</td>\n<td>+3</td>\n<td>+1</td>\n<td>-</td>\n</tr>\n<tr>\n<td>5th</td>\n<td>4</td>\n<td>+4</td>\n<td>4</td>\n<td>2</td>\n<td>+2</td>\n<td>+4</td>\n<td>+1</td>\n<td>Ability score increase</td>\n</tr>\n<tr>\n<td>6th</td>\n<td>5</td>\n<td>+5</td>\n<td>5</td>\n<td>3</td>\n<td>+2</td>\n<td>+4</td>\n<td>+1</td>\n<td>Devotion</td>\n</tr>\n<tr>\n<td>7th</td>\n<td>6</td>\n<td>+6</td>\n<td>6</td>\n<td>3</td>\n<td>+3</td>\n<td>+5</td>\n<td>+2</td>\n<td>-</td>\n</tr>\n<tr>\n<td>8th</td>\n<td>6</td>\n<td>+6</td>\n<td>6</td>\n<td>3</td>\n<td>+3</td>\n<td>+5</td>\n<td>+2</td>\n<td>-</td>\n</tr>\n<tr>\n<td>9th</td>\n<td>7</td>\n<td>+7</td>\n<td>7</td>\n<td>4</td>\n<td>+3</td>\n<td>+5</td>\n<td>+2</td>\n<td>Multiattack</td>\n</tr>\n<tr>\n<td>10th</td>\n<td>8</td>\n<td>+8</td>\n<td>8</td>\n<td>4</td>\n<td>+4</td>\n<td>+6</td>\n<td>+2</td>\n<td>Ability score increase</td>\n</tr>\n<tr>\n<td>11th</td>\n<td>9</td>\n<td>+9</td>\n<td>9</td>\n<td>5</td>\n<td>+4</td>\n<td>+6</td>\n<td>+3</td>\n<td>-</td>\n</tr>\n<tr>\n<td>12th</td>\n<td>9</td>\n<td>+9</td>\n<td>9</td>\n<td>5</td>\n<td>+4</td>\n<td>+6</td>\n<td>+3</td>\n<td>-</td>\n</tr>\n<tr>\n<td>13th</td>\n<td>10</td>\n<td>+10</td>\n<td>10</td>\n<td>5</td>\n<td>+5</td>\n<td>+7</td>\n<td>+3</td>\n<td>-</td>\n</tr>\n<tr>\n<td>14th</td>\n<td>11</td>\n<td>+11</td>\n<td>11</td>\n<td>6</td>\n<td>+5</td>\n<td>+7</td>\n<td>+3</td>\n<td>Improved evasion</td>\n</tr>\n<tr>\n<td>15th</td>\n<td>12</td>\n<td>+12</td>\n<td>12</td>\n<td>6</td>\n<td>+6</td>\n<td>+8</td>\n<td>+4</td>\n<td>Ability score increase</td>\n</tr>\n<tr>\n<td>16th</td>\n<td>12</td>\n<td>+12</td>\n<td>12</td>\n<td>6</td>\n<td>+6</td>\n<td>+8</td>\n<td>+4</td>\n<td>-</td>\n</tr>\n<tr>\n<td>17th</td>\n<td>13</td>\n<td>+13</td>\n<td>13</td>\n<td>7</td>\n<td>+6</td>\n<td>+8</td>\n<td>+4</td>\n<td>-</td>\n</tr>\n<tr>\n<td>18th</td>\n<td>14</td>\n<td>+14</td>\n<td>14</td>\n<td>7</td>\n<td>+7</td>\n<td>+9</td>\n<td>+4</td>\n<td>-</td>\n</tr>\n<tr>\n<td>19th</td>\n<td>15</td>\n<td>+15</td>\n<td>15</td>\n<td>8</td>\n<td>+7</td>\n<td>+9</td>\n<td>+5</td>\n<td>-</td>\n</tr>\n<tr>\n<td>20th</td>\n<td>15</td>\n<td>+15</td>\n<td>15</td>\n<td>8</td>\n<td>+7</td>\n<td>+9</td>\n<td>+5</td>\n<td>-</td>\n</tr>\n<tr>\n<td>21st</td>\n<td>16</td>\n<td>+16</td>\n<td>16</td>\n<td>8</td>\n<td>+8</td>\n<td>+10</td>\n<td>+5</td>\n<td>Ability score increase</td>\n</tr>\n<tr>\n<td>22nd</td>\n<td>17</td>\n<td>+17</td>\n<td>17</td>\n<td>9</td>\n<td>+8</td>\n<td>+10</td>\n<td>+5</td>\n<td>-</td>\n</tr>\n<tr>\n<td>23rd</td>\n<td>18</td>\n<td>+18</td>\n<td>18</td>\n<td>9</td>\n<td>+9</td>\n<td>+11</td>\n<td>+6</td>\n<td>-</td>\n</tr>\n<tr>\n<td>24th</td>\n<td>18</td>\n<td>+18</td>\n<td>18</td>\n<td>9</td>\n<td>+9</td>\n<td>+11</td>\n<td>+6</td>\n<td>-</td>\n</tr>\n<tr>\n<td>25th</td>\n<td>19</td>\n<td>+19</td>\n<td>19</td>\n<td>10</td>\n<td>+9</td>\n<td>+11</td>\n<td>+6</td>\n<td>-</td>\n</tr>\n<tr>\n<td>26th</td>\n<td>20</td>\n<td>+20</td>\n<td>20</td>\n<td>10</td>\n<td>+10</td>\n<td>+12</td>\n<td>+6</td>\n<td>Ability score increase</td>\n</tr>\n<tr>\n<td>27th</td>\n<td>21</td>\n<td>+21</td>\n<td>21</td>\n<td>11</td>\n<td>+10</td>\n<td>+12</td>\n<td>+7</td>\n<td>-</td>\n</tr>\n<tr>\n<td>28th</td>\n<td>21</td>\n<td>+21</td>\n<td>21</td>\n<td>11</td>\n<td>+10</td>\n<td>+12</td>\n<td>+7</td>\n<td>-</td>\n</tr>\n<tr>\n<td>29th</td>\n<td>22</td>\n<td>+22</td>\n<td>22</td>\n<td>11</td>\n<td>+11</td>\n<td>+13</td>\n<td>+7</td>\n<td>-</td>\n</tr>\n<tr>\n<td>30th</td>\n<td>23</td>\n<td>+23</td>\n<td>23</td>\n<td>12</td>\n<td>+11</td>\n<td>+13</td>\n<td>+7</td>\n<td>-</td>\n</tr>\n<tr>\n<td>31st</td>\n<td>24</td>\n<td>+24</td>\n<td>24</td>\n<td>12</td>\n<td>+12</td>\n<td>+14</td>\n<td>+8</td>\n<td>Ability score increase</td>\n</tr>\n<tr>\n<td>32nd</td>\n<td>24</td>\n<td>+24</td>\n<td>24</td>\n<td>12</td>\n<td>+12</td>\n<td>+14</td>\n<td>+8</td>\n<td>-</td>\n</tr>\n<tr>\n<td>33rd</td>\n<td>25</td>\n<td>+25</td>\n<td>25</td>\n<td>13</td>\n<td>+12</td>\n<td>+14</td>\n<td>+8</td>\n<td>-</td>\n</tr>\n<tr>\n<td>34th</td>\n<td>26</td>\n<td>+26</td>\n<td>26</td>\n<td>13</td>\n<td>+13</td>\n<td>+15</td>\n<td>+8</td>\n<td>-</td>\n</tr>\n<tr>\n<td>35th</td>\n<td>27</td>\n<td>+27</td>\n<td>27</td>\n<td>14</td>\n<td>+13</td>\n<td>+15</td>\n<td>+9</td>\n<td>-</td>\n</tr>\n<tr>\n<td>36th</td>\n<td>27</td>\n<td>+27</td>\n<td>27</td>\n<td>14</td>\n<td>+13</td>\n<td>+15</td>\n<td>+9</td>\n<td>-</td>\n</tr>\n<tr>\n<td>37th</td>\n<td>28</td>\n<td>+28</td>\n<td>28</td>\n<td>14</td>\n<td>+14</td>\n<td>+16</td>\n<td>+9</td>\n<td>Ability score increase</td>\n</tr>\n<tr>\n<td>38th</td>\n<td>29</td>\n<td>+29</td>\n<td>29</td>\n<td>15</td>\n<td>+14</td>\n<td>+16</td>\n<td>+9</td>\n<td>-</td>\n</tr>\n<tr>\n<td>39th</td>\n<td>30</td>\n<td>+30</td>\n<td>30</td>\n<td>15</td>\n<td>+15</td>\n<td>+17</td>\n<td>+10</td>\n<td>-</td>\n</tr>\n<tr>\n<td>40th</td>\n<td>30</td>\n<td>+30</td>\n<td>30</td>\n<td>15</td>\n<td>+15</td>\n<td>+17</td>\n<td>+10</td>\n<td>-</td>\n</tr>\n</table>\n<p><strong>Avian</strong><br>\n<strong>Size</strong> Medium; <strong>Speed</strong> 20 ft., Fly 15 ft*. (average); <strong>AC</strong> +2 natural armor; <strong>Saves</strong> Fort (good), Ref (good), Will (bad); <strong>Attack</strong> Bite (primary, 1d4 Medium, 1d3 Small), 2 talons (primary, 1d4 Medium, 1d3 Small, creature must be airborne to use); <strong>Str</strong> 12, <strong>Dex</strong> 16, <strong>Con</strong> 13, <strong>Int</strong> 7, <strong>Wis</strong> 10, <strong>Cha</strong> 11</p>\n<p>Avian creatures are birds and bird-like magical beasts and outsiders. An avian companion has a head, 2 legs, and 2 wings</p>\n<p>*This fly speed only functions on the companion&#8217;s turn. If the companion is not on a surface that can support it on the end of its turn, it glides to the ground, taking no falling damage. If the companion gains a natural fly speed from the Avian Creature (form) talent, increase the maneuverability of that speed by 1 step.</p>\n<p><strong>Biped</strong><br>\n<strong>Size</strong> Medium; <strong>Speed</strong> 30 ft.; <strong>AC</strong> +2 natural armor; <strong>Saves</strong> Fort (good), Ref (bad), Will (good); <strong>Attack</strong> 2 slams (1d4); <strong>Str</strong> 16, <strong>Dex</strong> 12, <strong>Con</strong> 13, <strong>Int</strong> 7, <strong>Wis</strong> 10, <strong>Cha</strong> 11</p>\n<p>Bipeds are usually humanoids, and begin with 2 legs, 2 arms, and a head.</p>\n<p><strong>Ooze</strong><br>\n<strong>Size</strong> Medium; <strong>Speed</strong> 20 ft.; <strong>AC</strong> +4 natural armor; <strong>Saves</strong> Fort (good), Ref (bad), Will (bad); <strong>Attack</strong> slam (primary, 1d6 Medium, 1d4 Small); <strong>Str</strong> 16, <strong>Dex</strong> 8, <strong>Con</strong> 16, <strong>Int</strong> 7, <strong>Wis</strong> 12, <strong>Cha</strong> 11.</p>\n<p>Ooze creatures are usually oozes, puddings, or other amorphous creatures and lack discernable limbs. An ooze companion may not be tripped unless gaining legs from another source.</p>\n<p><strong>Orb</strong><br>\n<strong>Size</strong> Medium; <strong>Speed</strong> 5 ft. Hover*, 30 ft. (average); <strong>AC</strong> +2 natural armor; <strong>Saves</strong> Fort (bad), Ref (good), Will (good); <strong>Attack</strong> bite or slam (choose 1) (1d6); <strong>Str</strong> 7, <strong>Dex</strong> 16, <strong>Con</strong> 13, <strong>Int</strong> 10^,<strong>Wis</strong> 12, <strong>Cha</strong> 11.</p>\n<p>Orb creatures are lantern archons, will-o-wisps, gibbering orbs, and other mystical fey, outsiders, constructs, or aberrations with a floating sphere-like appearance and supernatural movement. An orb lacks any limbs, but may treat its body as a head for the purpose of adding natural attacks. An orb companion may not be tripped unless gaining legs from another source.<br>\n*An orb may float up to 5 feet plus 5 feet per 5 Hit Dice above the ground, with a horizontal movement speed of 30 feet. When floating this way, Fly checks are not required to hover or change direction. When falling the orb may choose to descend at a slower rate to control its fall and to negate all falling damage it would take. Each round it descends 30 feet, and may move in another direction for 30 feet. It may choose to drift sideways, gliding forwards while descending, or down, safely increasing its rate of descent. It may even choose to drift &#8216;upwards&#8217; to reduce its rate of descent, even allowing it to negate it entirely and hover midair (though cannot move horizontally if it begins its turn doing so). This is a supernatural ability.<br>\n^The Skillful Companion (form) talent increases Int to 13 if taken.</p>\n<p><strong>Quadruped</strong><br>\n<strong>Size</strong> Medium; <strong>Speed</strong> 40 ft.; <strong>AC</strong> +2 natural armor; <strong>Saves</strong> Fort (good), Ref (good), Will (bad); <strong>Attack</strong> bite (1d6); <strong>Str</strong> 14, <strong>Dex</strong> 14, <strong>Con</strong> 13, <strong>Int</strong> 7, <strong>Wis</strong> 10, <strong>Cha</strong> 11</p>\n<p>Quadrupeds are usually beasts, and begin with 4 legs and a head.</p>\n<p><strong>Serpentine</strong><br>\n<strong>Size</strong> Medium; <strong>Speed</strong> 20 ft.; <strong>AC</strong> +4 natural armor; <strong>Saves</strong> Fort (bad), Ref (good), Will (good); <strong>Attack</strong> bite (1d6), tail slap (1d6); <strong>Str</strong> 12, <strong>Dex</strong> 16, <strong>Con</strong> 13, <strong>Int</strong> 7, <strong>Wis</strong> 10, <strong>Cha</strong> 11</p>\n<p>Serpentine creatures are snakes, fish, and other elongated creatures. They begin with a head, but no arms or legs.</p>\n<p><strong>Vermin</strong><br>\n<strong>Size</strong> Medium; <strong>Speed</strong> 20 ft., Climb 20 ft.; <strong>AC</strong> +2 natural armor; <strong>Saves</strong> Fort (good), Ref (good), Will (bad); <strong>Attack</strong> bite (1d6); <strong>Str</strong> 12, <strong>Dex</strong> 16, <strong>Con</strong> 13, <strong>Int</strong> 7, <strong>Wis</strong> 10, <strong>Cha</strong> 11</p>\n<p>Vermin are usually insects or arachnids, and begin with either 6 or 8 legs and a head and gain a +6 bonus to CMD vs. trip attempts from its additional legs.</p>\n<h3 id=\"toc1\"><span>Companion Features</span></h3>\n<p>You may choose to make a companion Small-sized instead of Medium-sized. In this case, the companion gains a +2 bonus to Dexterity, a -2 penalty to Strength, as well as the usual changes for being Small (+4 Stealth bonus, +1 AC, +1 to-hit, decreased damage size, etc.).</p>\n<p>A companion gains 2 skill points per level (reduced to 1 for low Intelligence) and gains the following class skills: Climb (Str), Fly (Dex), Knowledge (planes) (Int), Stealth (Dex), Swim (Str). They possess a d10 Hit Die and gain 2 good saves and 1 bad save dependent on the creature&#8217;s form. A companion begins understanding and speaking one language that the caster also speaks.</p>\n<p><strong>Feats:</strong> A companion begins with one feat, and gains another feat at every odd Hit Die. A companion may gain any PC or monster feat for which it qualifies. While a companion may gain casting abilities through feats such as Basic Magic Training or Advanced Magic Training, a companion can never possess the Conjuration sphere.</p>\n<p><strong>Evasion:</strong> At 2 Hit Dice, a companion gains evasion, taking no damage on a successful Reflex save against an effect that grants half damage on a successful Reflex save. At 11 Hit Dice, they gain improved evasion, only take half damage on failed Reflex saves against effects that grant half damage on a successful Reflex save.</p>\n<p><strong>Ability Score Increase:</strong> Just like PCs and monsters, a companion gains a permanent +1 bonus to an ability score of the caster&#8217;s choice for every 4 Hit Dice possessed.</p>\n<p><strong>Devotion:</strong> At 5 Hit Dice, a companion gains a +4 morale bonus on Will saves against charm and enchantment effects, such as enchantment school spells or the Mind sphere.</p>\n<p><strong>Multiattack:</strong> At 7 Hit Dice, a companion gains Multiattack as a bonus feat if it has 3 or more natural attacks and does not already have that feat. If it does not have the requisite 3 or more natural attacks (or it is reduced to less than 3 attacks), the companion instead gains a second attack with one of its natural weapons, albeit at a &#8211;5 penalty. If the companion later gains 3 or more natural attacks, it loses this additional attack and instead gains Multiattack.</p>\n",
      "unidentified": ""
    },
    "tags": [],
    "actions": [],
    "uses": {
      "per": null,
      "value": 0,
      "maxFormula": "",
      "autoDeductChargesCost": "1"
    },
    "attackNotes": [],
    "effectNotes": [],
    "changes": [],
    "changeFlags": {
      "loseDexToAC": false,
      "noEncumbrance": false,
      "mediumArmorFullSpeed": false,
      "heavyArmorFullSpeed": false
    },
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "tag": "",
    "useCustomTag": false,
    "armorProf": {
      "value": [],
      "custom": ""
    },
    "weaponProf": {
      "value": [],
      "custom": ""
    },
    "languages": {
      "value": [],
      "custom": ""
    },
    "flags": {
      "boolean": {},
      "dictionary": {}
    },
    "scriptCalls": [],
    "subType": "magicTalent",
    "associations": {
      "classes": []
    },
    "showInQuickbar": false,
    "abilityType": "none",
    "crOffset": "",
    "disabled": false,
    "classSkills": {},
    "nonlethal": false,
    "duration": {
      "value": null,
      "units": ""
    },
    "target": {
      "value": ""
    },
    "attackName": "",
    "actionType": null,
    "attackBonus": "",
    "critConfirmBonus": "",
    "damage": {
      "parts": [],
      "critParts": [],
      "nonCritParts": []
    },
    "attackParts": [],
    "formulaicAttacks": {
      "count": {
        "formula": ""
      },
      "bonus": {
        "formula": ""
      },
      "label": null
    },
    "formula": "",
    "ability": {
      "attack": null,
      "damage": null,
      "damageMult": 1,
      "critRange": 20,
      "critMult": 2
    },
    "save": {
      "dc": 0,
      "type": "",
      "description": ""
    },
    "soundEffect": "",
    "powerAttack": {
      "multiplier": "",
      "damageBonus": 2,
      "critMultiplier": 1
    }
  },
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "9.0",
    "coreVersion": "10.291",
    "createdTime": 1663168890496,
    "modifiedTime": 1689623922771,
    "lastModifiedBy": "pf1spheres-db000"
  }
}
