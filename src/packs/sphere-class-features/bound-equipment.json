{
  "_id": "a4TUSEOKc5b9M4K9",
  "name": "Bound Equipment",
  "type": "feat",
  "img": "icons/svg/item-bag.svg",
  "effects": [],
  "folder": null,
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p>An armorist gains a bond with a specific weapon, shield, or suit of armor that is far greater than with her other summoned equipment. Bound equipment is summoned and treated just like summoned equipment, except it does not cost a spell point to summon, cannot be used by anyone other than the armorist herself (the item&#8217;s magic does not function in another&#8217;s hands), comes with limitless ammunition, and disappears 1 round after leaving the armorist&#8217;s hands. She gains another piece of bound equipment at 5th level, 10th level, 15th level, and 20th level, and may bond implements beginning at 5th level.</p>\n<p>Bound equipment is masterwork quality, and gains a +1 enhancement bonus for every odd armorist level possessed. A piece of bound equipment&#8217;s enhancement bonus cannot exceed +1, +1 per 3 armorist levels (maximum +5); all enhancement bonus beyond this must be traded for special abilities.</p>\n<p>An armorist may give her bound weapon special abilities from Table: Bound Equipment, trading enhancement bonus for other benefits. Bound equipment cannot be enchanted through any other means than gaining armorist levels and must retain at least a +1 enhancement bonus. Bound implements use the rules presented in the Magic Items chapter, granting the armorist enhancement bonuses to her caster level with one sphere, chosen when the implement is created. A piece of bound equipment may be an implement or a weapon/shield/suit of armor, but not both at the same time.</p>\n<p>Once a piece of bound equipment has been selected, or once its special abilities have been chosen, they may only be changed if the armorist spends 8 hours (1 day) meditating to dismiss the old piece of bound equipment and create a new piece of bound equipment to take its place.</p>\n<p>The armorist&#8217;s bound equipment can be damaged and sundered, but always returns to full health the following day.</p>\n<p>A bound double-weapon must divide its enhancement bonuses and special abilities between the two ends (for example, a +5 bound quarterstaff could have a +3 bonus on one end and a +2 bonus on the other). Alternatively, the armorist may bind each end of the double weapon as a separate piece of bound equipment, thus granting full bonuses to each half. If one end of the double weapon has no bonuses, it is still considered masterwork.</p>\n<p><strong>Author's Note:</strong> A bound implement may take any shape a normal implement could (e.g. as a held item, magic item slot, or slotless).</p>\n<p><strong>Wiki Note:</strong> Most of the following abilities can be found on one of these pages:</p>\n<ul>\n<li><a href=\"https://aonprd.com/MagicWeapons.aspx?Category=MeleeWeaponQuality\">Melee Weapon Qualities</a> (Archives of Nethys)</li>\n<li><a href=\"https://aonprd.com/MagicWeapons.aspx?Category=RangedWeaponQuality\">Ranged Weapon Qualities</a> (Archives of Nethys)</li>\n<li><a href=\"https://aonprd.com/MagicArmor.aspx?Category=ArmorQuality\">Armor Qualities</a> (Archives of Nethys)</li>\n<li><a href=\"https://aonprd.com/MagicArmor.aspx?Category=ShieldQuality\">Shield Qualities</a> (Archives of Nethys)</li>\n<li><a href=\"http://spheresofpower.wikidot.com/weapons\">Weapons</a></li>\n<li><a href=\"http://spheresofpower.wikidot.com/armor\">Armor</a></li>\n</ul>\n<strong>Table: Bound Equipment</strong><br>\n<table class=\"wiki-content-table\">\n<tr>\n<th>Weapon Special Ability</th>\n<th>Enhance. Bonus Value</th>\n<th>Shield Special Ability</th>\n<th>Enhance. Bonus Value</th>\n<th>Armor Special Ability</th>\n<th>Enhance. Bonus Value</th>\n<th>Implement Special Ability</th>\n<th>Enhance. Bonus Value</th>\n</tr>\n<tr>\n<td>Advancing</td>\n<td>+2 bonus</td>\n<td>Arrow catching</td>\n<td>+1 bonus</td>\n<td>Anti-ballistic</td>\n<td>+1 bonus</td>\n<td>Abjuring</td>\n<td>+3 bonus</td>\n</tr>\n<tr>\n<td>Agile</td>\n<td>+1 bonus</td>\n<td>Arrow deflection</td>\n<td>+2 bonus</td>\n<td>Anti-spell</td>\n<td>+2 bonus</td>\n<td>Aggressive</td>\n<td>+1 bonus</td>\n</tr>\n<tr>\n<td>Anchoring</td>\n<td>+2 bonus</td>\n<td>Bashing</td>\n<td>+1 bonus</td>\n<td>Benevolent</td>\n<td>+1 bonus</td>\n<td>Aiming</td>\n<td>+1 bonus</td>\n</tr>\n<tr>\n<td>Arcing</td>\n<td>+1 bonus</td>\n<td>Blinding</td>\n<td>+1 bonus</td>\n<td>Bolstering</td>\n<td>+1 bonus</td>\n<td>Alerting</td>\n<td>+3 bonus</td>\n</tr>\n<tr>\n<td>Avalanche</td>\n<td>+1 bonus</td>\n<td>Clangorous</td>\n<td>+1 bonus</td>\n<td>Brawling</td>\n<td>+3 bonus</td>\n<td>Bloodhound</td>\n<td>+2 bonus</td>\n</tr>\n<tr>\n<td>Benevolent</td>\n<td>+1 bonus</td>\n<td>Deflecting</td>\n<td>+1 bonus</td>\n<td>Deathless</td>\n<td>+1 bonus</td>\n<td>Capacitance</td>\n<td>+2 bonus</td>\n</tr>\n<tr>\n<td>Blast vessel</td>\n<td>+2 bonus</td>\n<td>Focusing</td>\n<td>+4 bonus</td>\n<td>Energy resistance (10, one element)</td>\n<td>+3 bonus</td>\n<td>Carved</td>\n<td>+1 bonus</td>\n</tr>\n<tr>\n<td>Corrosive</td>\n<td>+1 bonus</td>\n<td>Fortification (heavy)</td>\n<td>+5 bonus</td>\n<td>Energy resistance, improved (20, one element)</td>\n<td>+4 bonus</td>\n<td>Equitable</td>\n<td>+1 bonus</td>\n</tr>\n<tr>\n<td>Corrosive Burst</td>\n<td>+2 bonus</td>\n<td>Fortification (light)</td>\n<td>+1 bonus</td>\n<td>Energy resistance, greater (30, one element)</td>\n<td>+5 bonus</td>\n<td>Extra sphere</td>\n<td>+1 bonus</td>\n</tr>\n<tr>\n<td>Courageous</td>\n<td>+1 bonus</td>\n<td>Fortification (moderate)</td>\n<td>+3 bonus</td>\n<td>Focusing</td>\n<td>+4 bonus</td>\n<td>Girding</td>\n<td>+2 bonus</td>\n</tr>\n<tr>\n<td>Cruel</td>\n<td>+1 bonus</td>\n<td>Ghost touch</td>\n<td>+3 bonus</td>\n<td>Fortification (heavy)</td>\n<td>+5 bonus</td>\n<td>Macabre</td>\n<td>+1 bonus</td>\n</tr>\n<tr>\n<td>Cunning</td>\n<td>+1 bonus</td>\n<td>Grinding</td>\n<td>+1 bonus</td>\n<td>Fortification (light)</td>\n<td>+1 bonus</td>\n<td>Magic talent</td>\n<td>+2 bonus</td>\n</tr>\n<tr>\n<td>Decisive</td>\n<td>+2 bonus</td>\n<td>Impervious</td>\n<td>+1 bonus</td>\n<td>Fortification (moderate)</td>\n<td>+3 bonus</td>\n<td>Mesmerism</td>\n<td>+3 bonus</td>\n</tr>\n<tr>\n<td>Defending</td>\n<td>+1 bonus</td>\n<td>Merging</td>\n<td>+2 bonus</td>\n<td>Ghost touch</td>\n<td>+3 bonus</td>\n<td>Reaving (normal)</td>\n<td>+1 bonus</td>\n</tr>\n<tr>\n<td>Destructive focus</td>\n<td>+1 bonus</td>\n<td>Mirrored</td>\n<td>+1 bonus</td>\n<td>Grinding</td>\n<td>+1 bonus</td>\n<td>Reaving (greater)</td>\n<td>+3 bonus</td>\n</tr>\n<tr>\n<td>Destructive focus, greater</td>\n<td>+2 bonus</td>\n<td>Ramming</td>\n<td>+1 bonus</td>\n<td>Invulnerability</td>\n<td>+3 bonus</td>\n<td>Sanguine</td>\n<td>+1 bonus</td>\n</tr>\n<tr>\n<td>Dimensional</td>\n<td>+4 bonus</td>\n<td>Reflecting</td>\n<td>+5 bonus</td>\n<td>Impervious</td>\n<td>+1 bonus</td>\n<td>Sunset</td>\n<td>+1 bonus</td>\n</tr>\n<tr>\n<td>Distance</td>\n<td>+1 bonus</td>\n<td>Spell resistance (13)</td>\n<td>+2 bonus</td>\n<td>Mind buttressing</td>\n<td>+2 bonus</td>\n<td>Sustaining</td>\n<td>+2 bonus</td>\n</tr>\n<tr>\n<td>Driving</td>\n<td>+1 bonus</td>\n<td>Spell resistance (15)</td>\n<td>+3 bonus</td>\n<td>Mirrored</td>\n<td>+1 bonus</td>\n<td>Vital</td>\n<td>+2 bonus</td>\n</tr>\n<tr>\n<td>Entangling</td>\n<td>+1 bonus</td>\n<td>Spell resistance (17)</td>\n<td>+4 bonus</td>\n<td>Shadow warded</td>\n<td>+1 bonus</td>\n<td>Watchful</td>\n<td>+2 bonus</td>\n</tr>\n<tr>\n<td>Fey-Forged</td>\n<td>+1 bonus</td>\n<td>Spell resistance (19)</td>\n<td>+5 bonus</td>\n<td>Stanching</td>\n<td>+1 bonus</td>\n<td>Wellspring</td>\n<td>+1 bonus</td>\n</tr>\n<tr>\n<td>Flaming</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td>Spell dodging</td>\n<td>+2 bonus</td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Flaming Burst</td>\n<td>+2 bonus</td>\n<td></td>\n<td></td>\n<td>Spell resistance (13)</td>\n<td>+2 bonus</td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Frost</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td>Spell resistance (15)</td>\n<td>+3 bonus</td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Ghost touch</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td>Spell resistance (17)</td>\n<td>+4 bonus</td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Howling</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td>Spell resistance (19)</td>\n<td>+5 bonus</td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Hungry</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td>Supplying</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Hypochondriac</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td>Titanic</td>\n<td>+3 bonus</td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Icy burst</td>\n<td>+2 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Impervious</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Invisible</td>\n<td>+3 bonus (+4 firearms)</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Keen</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Leaping</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Menacing</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Merciful</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Mighty cleaving</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Preventative</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Quenching</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Radiant edge</td>\n<td>+2 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Reposition</td>\n<td>+3 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Returning</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Shadow wake</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Shadowstrike</td>\n<td>+2 (+3 ranged)</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Shock</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Shocking burst</td>\n<td>+2 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Speed</td>\n<td>+3 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Thundering</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Throwing</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Umbral edge</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Vicious</td>\n<td>+1 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Vorpal</td>\n<td>+5 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Windblast</td>\n<td>+2 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n<tr>\n<td>Wounding</td>\n<td>+2 bonus</td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n</tr>\n</table>\n",
      "unidentified": ""
    },
    "tags": [],
    "actions": [],
    "uses": {
      "per": null,
      "value": 0,
      "maxFormula": "",
      "autoDeductChargesCost": "1"
    },
    "attackNotes": [],
    "effectNotes": [],
    "changes": [],
    "changeFlags": {
      "loseDexToAC": false,
      "noEncumbrance": false,
      "mediumArmorFullSpeed": false,
      "heavyArmorFullSpeed": false
    },
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "tag": "",
    "useCustomTag": false,
    "armorProf": {
      "value": [],
      "custom": ""
    },
    "weaponProf": {
      "value": [],
      "custom": ""
    },
    "languages": {
      "value": [],
      "custom": ""
    },
    "flags": {
      "boolean": {},
      "dictionary": {}
    },
    "scriptCalls": [],
    "subType": "classFeat",
    "associations": {
      "classes": []
    },
    "showInQuickbar": false,
    "abilityType": "su",
    "crOffset": "",
    "disabled": false,
    "classSkills": {},
    "nonlethal": false,
    "duration": {
      "value": null,
      "units": ""
    },
    "target": {
      "value": ""
    },
    "attackName": "",
    "actionType": null,
    "attackBonus": "",
    "critConfirmBonus": "",
    "damage": {
      "parts": [],
      "critParts": [],
      "nonCritParts": []
    },
    "attackParts": [],
    "formulaicAttacks": {
      "count": {
        "formula": ""
      },
      "bonus": {
        "formula": ""
      },
      "label": null
    },
    "formula": "",
    "ability": {
      "attack": null,
      "damage": null,
      "damageMult": 1,
      "critRange": 20,
      "critMult": 2
    },
    "save": {
      "dc": 0,
      "type": "",
      "description": ""
    },
    "soundEffect": ""
  },
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "9.0",
    "coreVersion": "10.291",
    "createdTime": 1663168900702,
    "modifiedTime": 1689623926374,
    "lastModifiedBy": "pf1spheres-db000"
  }
}
