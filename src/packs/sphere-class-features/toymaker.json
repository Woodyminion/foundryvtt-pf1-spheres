{
  "_id": "roSrow0gRFu0AseY",
  "name": "Toymaker",
  "type": "feat",
  "img": "icons/svg/item-bag.svg",
  "effects": [],
  "folder": null,
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p>The blacksmith gains a familiar, as the wizard&#8217;s arcane bond option, using his blacksmith levels as wizard levels for this purpose, as well as Object Familiar (Ultimate Spheres of Power pg. 481) as a bonus feat, even if he does not meet the prerequisites.</p>\n<p>The blacksmith must select a Tiny animated object in place of a normal creature whenever he could select a familiar, and the familiar gains a toy-like appearance. The blacksmith can choose any special ability a normal familiar could grant its master (such as a fox&#8217;s +2 bonus on Reflex saves or a cat&#8217;s +3 bonus on Stealth checks) and gains that bonus as though his animated object familiar were a familiar of that kind.</p>\n<p>The blacksmith may use his reforge ability on his familiar, spending 1 hour to change its appearance, so long as it retains a toy-like appearance. When he does this, the blacksmith may reselect the special ability his animated object grants him as a familiar.</p>\n<h2 id=\"toc43\"><span><span style=\"color: #993300\">Artisan Savant</span></span></h2>\n<p>Starting at 3rd level, the blacksmith becomes proficient in creating powerful magic equipment, treating his class level as his caster level for crafting feats and abilities and using the appropriate Craft skill (weapons, armor, bows, etc.) for the check to complete the item. In addition, the blacksmith gains Craft Wondrous Item as a bonus feat at 3rd level, and Craft Magic Arms and Armor at 5th level.</p>\n<p><strong>Note:</strong> If using Spheres of Power, you may exchange these feats for Craft Marvelous Items and Smith Magical Weapons And Armor, respectively.</p>\n<h2 id=\"toc44\"><span><span style=\"color: #993300\">Reforge (Ex)</span></span></h2>\n<p>From 3rd level on, the blacksmith&#8217;s experience with crafting excellent equipment allows him to manipulate and rework magical equipment without damaging or sacrificing its magical potency. The blacksmith can spend 1 hour per total effective bonus (minimum 1 hour) of an enchanted armor, weapon, or shield (enhancement bonus plus permanent special ability bonus equivalents) at an operational forge to reforge the equipment into another equivalent item at no additional cost; weapons into weapons of another type, armor into a separate type of armor, and shields into weapons, armor, or other types of shields; a heavy shield can be converted into a light shield or vice versa, and a tower shield can be converted into any other type of shield, but bucklers, light shields and heavy shields cannot be converted into tower shields. When reforging, the blacksmith cannot exchange one piece of equipment for another that is not predominantly made out of the same dominant material; a greatclub can be reforged into a quarterstaff or spear, but not into a bastard sword, and a suit of platemail could be reforged into a breastplate or chain shirt, but not into a suit of leather armor. The new weapon or armor must be the same size or smaller as the original.</p>\n<p>In addition, the blacksmith can restore a magic weapon, armor, or shield that has been destroyed by a sunder attack or similar ability; if the blacksmith can acquire all the broken pieces of the equipment, he may restore it to its original complete condition, including all enchantments, with 8 hours of work at an operational forge.</p>\n<h2 id=\"toc45\"><span><span style=\"color: #993300\">Rapid Maintenance (Ex)</span></span></h2>\n<p>The blacksmith&#8217;s skill at performing maintenance improves and he may now perform multiple maintenances at once. At 9th, 13th, and 17th level, the blacksmith may perform 1 additional maintenance during an 8 hour rest (for a total of 4 maintenances per affected creature at 17th level).</p>\n<h2 id=\"toc46\"><span><span style=\"color: #993300\">Smith&#8217;s Masterpiece</span></span></h2>\n<p>At 20th level the blacksmith reaches the pinnacle of his craft. Throughout his career, the blacksmith has collected and hoarded special materials, crafting secrets, and scraps of ancient rituals, all in preparation for his ultimate life&#8217;s work. The blacksmith crafts a single magic armor, weapon, or shield of his choice, imbuing it with power beyond what any normal piece of equipment can contain. The crafted item has a +5 enhancement bonus, and the smith may choose up to +8 effective enhancement bonus&#8217; worth of special abilities to apply to this piece of equipment; once chosen, these enhancements cannot be changed. This ability supersedes the normal restriction preventing equipment from exceeding a total enhancement bonus of +10 including class abilities. As long as the blacksmith and his masterpiece are on the same plane, the blacksmith can recall the masterpiece to himself with 1 minute of concentration, even if it is currently in another creature&#8217;s possession.</p>\n",
      "unidentified": ""
    },
    "tags": [],
    "actions": [],
    "uses": {
      "per": null,
      "value": 0,
      "maxFormula": "",
      "autoDeductChargesCost": "1"
    },
    "attackNotes": [],
    "effectNotes": [],
    "changes": [],
    "changeFlags": {
      "loseDexToAC": false,
      "noEncumbrance": false,
      "mediumArmorFullSpeed": false,
      "heavyArmorFullSpeed": false
    },
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "tag": "",
    "useCustomTag": false,
    "armorProf": {
      "value": [],
      "custom": ""
    },
    "weaponProf": {
      "value": [],
      "custom": ""
    },
    "languages": {
      "value": [],
      "custom": ""
    },
    "flags": {
      "boolean": {},
      "dictionary": {}
    },
    "scriptCalls": [],
    "subType": "classFeat",
    "associations": {
      "classes": []
    },
    "showInQuickbar": false,
    "abilityType": "ex",
    "crOffset": "",
    "disabled": false,
    "classSkills": {},
    "nonlethal": false,
    "duration": {
      "value": null,
      "units": ""
    },
    "target": {
      "value": ""
    },
    "attackName": "",
    "actionType": null,
    "attackBonus": "",
    "critConfirmBonus": "",
    "damage": {
      "parts": [],
      "critParts": [],
      "nonCritParts": []
    },
    "attackParts": [],
    "formulaicAttacks": {
      "count": {
        "formula": ""
      },
      "bonus": {
        "formula": ""
      },
      "label": null
    },
    "formula": "",
    "ability": {
      "attack": null,
      "damage": null,
      "damageMult": 1,
      "critRange": 20,
      "critMult": 2
    },
    "save": {
      "dc": 0,
      "type": "",
      "description": ""
    },
    "soundEffect": ""
  },
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "9.0",
    "coreVersion": "10.291",
    "createdTime": 1663168900987,
    "modifiedTime": 1689623926533,
    "lastModifiedBy": "pf1spheres-db000"
  }
}
